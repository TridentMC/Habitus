/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.proxy;

import com.tridevmc.habitus.Habitus;
import com.tridevmc.habitus.block.BlockLeavesBase;
import com.tridevmc.habitus.init.HSEntities;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;

public class ClientProxy extends CommonProxy {

    @Override
    public void init() {
        super.init();
        HSEntities.registerEntityRenderer();
    }

    @Override
    public void registerItemRenderer(Item item, int meta, String id) {
        this.registerItemRenderer(item, meta, id, "inventory");
    }

    @Override
    public void setGraphicsLevel(BlockLeavesBase leaves, boolean fancy) {
        leaves.setGraphicsLevel(fancy);
    }

    @Override
    public void registerItemRenderer(Item item, int meta, String id, String variant) {
        ModelLoader.setCustomModelResourceLocation(item, meta,
            new ModelResourceLocation(Habitus.MOD_ID + ":" + id, variant));
    }

}
