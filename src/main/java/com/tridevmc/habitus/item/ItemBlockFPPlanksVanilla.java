package com.tridevmc.habitus.item;

import com.tridevmc.habitus.init.HSBlocks;
import net.minecraft.block.BlockPlanks;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.ItemMultiTexture;
import net.minecraft.item.ItemStack;

public class ItemBlockFPPlanksVanilla extends ItemMultiTexture {
    public ItemBlockFPPlanksVanilla() {
        super(HSBlocks.fpPlanksVanilla, HSBlocks.fpPlanksVanilla,
                stack -> "fp_planks_" + BlockPlanks.EnumType.byMetadata(stack.getMetadata()).getUnlocalizedName());
    }

    @Override
    public String getItemStackDisplayName(ItemStack stack) {
        return I18n.format("tile.habitus.fireproof",
                I18n.format("tile.wood." + BlockPlanks.EnumType.byMetadata(stack.getMetadata()).getUnlocalizedName() + ".name"));
    }
}
