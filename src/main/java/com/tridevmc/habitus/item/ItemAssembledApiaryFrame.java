/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.item;

import java.util.List;
import javax.annotation.Nullable;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class ItemAssembledApiaryFrame extends ItemBase {

    protected ItemAssembledApiaryFrame(String name) {
        super(name);
        this.setMaxStackSize(1);
    }

    public ItemAssembledApiaryFrame() {
        this("assembled_apiary_frame");
    }

    @Override
    protected boolean shouldAddToTab() {
        return false;
    }

    @Override
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip,
        ITooltipFlag flagIn) {
        NBTTagCompound compound = stack.getTagCompound();
        if (compound != null) {
            int size = compound.getCompoundTag("foundation").getInteger("size");
            int depth = compound.getCompoundTag("frame").getInteger("depth");
            tooltip.add("Frame Depth: " + depth);
            tooltip.add("Foundation Size: " + size);
        }
    }

    @Override
    public boolean hasEffect(ItemStack stack) {
        return true;
    }
}
