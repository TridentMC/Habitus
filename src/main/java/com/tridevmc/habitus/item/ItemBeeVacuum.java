/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.item;

import com.tridevmc.habitus.entity.EntityBaseSwarm;
import com.tridevmc.habitus.entity.EntityBeeSwarm;
import com.tridevmc.habitus.init.HSSounds;
import com.tridevmc.habitus.tile.BeePopulation;
import com.tridevmc.habitus.tile.ITEBeePopulation;
import javax.annotation.Nonnull;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;

public class ItemBeeVacuum extends ItemBase {

    public ItemBeeVacuum() {
        super("bee_vacuum");
        this.setMaxStackSize(1);
        this.setMaxDamage(25);
    }

    @Override
    @Nonnull
    public EnumActionResult onItemUse(EntityPlayer player, World worldIn, BlockPos pos,
        EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (worldIn.getTileEntity(pos) == null || !(worldIn
            .getTileEntity(pos) instanceof ITEBeePopulation)) {
            return EnumActionResult.FAIL;
        }

        ITEBeePopulation apiary = (ITEBeePopulation) worldIn.getTileEntity(pos);

        if (apiary != null) {
            ItemStack stack = player.inventory.getCurrentItem();
            if (stack == ItemStack.EMPTY) {
                return EnumActionResult.FAIL;
            } else if (!stack.hasTagCompound()) {
                if (apiary.hasPopulation()) {
                    BeePopulation population = apiary.getAndDestroyPopulation();
                    stack.setTagCompound(population.serializeToNBT());
                    return EnumActionResult.SUCCESS;
                }
                return EnumActionResult.FAIL;
            } else {
                NBTTagCompound population = stack.getTagCompound();
                BeePopulation beePopulation = new BeePopulation();
                beePopulation.readFromNBT(population);
                if (!apiary.hasPopulation()) {
                    EnumActionResult res = apiary.setPopulation(player, beePopulation);
                    if (res == EnumActionResult.SUCCESS) {
                        player.inventory.getCurrentItem().setTagCompound(null);
                    }
                    return res;
                }
                return EnumActionResult.FAIL;
            }
        }
        return EnumActionResult.FAIL;
    }

    @Override
    public boolean itemInteractionForEntity(ItemStack stack, EntityPlayer playerIn,
        EntityLivingBase target, EnumHand hand) {
        if (target.world.isRemote) {
            if (!stack.hasTagCompound() && target instanceof EntityBeeSwarm) {
                playerIn.playSound(HSSounds.vacuum_use, 1.0f, 1.0f);
            }

            return false;
        }

        if (target instanceof EntityBaseSwarm) {
            if(!(target instanceof EntityBeeSwarm)) {
                playerIn.sendStatusMessage(new TextComponentTranslation("item.habitus.bee_vacuum.badBees"),
                                true);
                return false;
            }

            if (stack.hasTagCompound()) {
                playerIn
                        .sendStatusMessage(new TextComponentTranslation("item.habitus.bee_vacuum.gotBees"),
                                true);
                return false;
            }

            EntityBeeSwarm swarm = ((EntityBeeSwarm) target);
            BeePopulation population = swarm.getBeePopulation();
            swarm.setDead();

            stack.setTagCompound(population.serializeToNBT());
            stack.setItemDamage(stack.getItemDamage() + 1);

            return true;
        }

        return false;
    }

    @Override
    public boolean hasEffect(ItemStack stack) {
        return stack.hasTagCompound();
    }

}
