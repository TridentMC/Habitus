/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.item;

import java.util.List;
import javax.annotation.Nullable;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ItemAssembledBalancedApiaryFrame extends ItemAssembledApiaryFrame {

    public ItemAssembledBalancedApiaryFrame() {
        super("assembled_balanced_apiary_frame");
    }

    @Override
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip,
        ITooltipFlag flagIn) {
        tooltip.add("§5Totally balanced. Volatile.");
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn,
        EnumHand handIn) {
        if (!worldIn.isRemote) {
            BlockPos playerPos = playerIn.getPosition();
            worldIn.createExplosion(null, playerPos.getX(), playerPos.getY(), playerPos.getZ(), 3,
                true);
        }
        ItemStack heldStack = playerIn.getHeldItem(handIn);
        heldStack.shrink(1);
        return ActionResult.newResult(EnumActionResult.SUCCESS, heldStack);
    }
}
