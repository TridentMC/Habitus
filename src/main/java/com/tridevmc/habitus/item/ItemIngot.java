/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.item;

import com.tridevmc.habitus.generic.IOreDict;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;

public class ItemIngot extends ItemBase implements IOreDict {

    private final String oredict;

    public ItemIngot(String name, String oredict) {
        super(name);
        this.oredict = oredict;
    }

    @Override
    public void registerOreDict() {
        OreDictionary.registerOre(oredict, new ItemStack(this, 1, OreDictionary.WILDCARD_VALUE));
    }
}
