/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.item;

import com.tridevmc.habitus.Habitus;
import net.minecraft.block.Block;
import net.minecraft.item.ItemDoor;

public abstract class ItemDoorBase extends ItemDoor implements IHabitusItem {

    protected final String name;

    public ItemDoorBase(Block block, String name) {
        super(block);
        this.name = name;
        this.setRegistryName(name + "_door");
        this.setUnlocalizedName("habitus." + name + "_door");
        this.setCreativeTab(Habitus.habitusTab);
    }

    public void registerItemModel() {
        Habitus.proxy.registerItemRenderer(this, 0, name + "_door");
    }
}
