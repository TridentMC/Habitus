/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.item;

import com.tridevmc.habitus.init.HSItems;
import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.NonNullList;
import net.minecraft.world.World;

public class ItemBalancedApiaryFrame extends ItemApiaryFrame {

    public ItemBalancedApiaryFrame() {
        super("balanced_apiary_frame");
    }

    @Override
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip,
        ITooltipFlag flagIn) {
        tooltip.add("§5Totally balanced. Fragile.");
    }

    private static ItemStack getFrame(int size) {
        ItemStack stack = new ItemStack(HSItems.balancedApiaryFrame, 1);
        NBTTagCompound compound = new NBTTagCompound();
        compound.setInteger("depth", size);
        stack.setTagCompound(compound);
        return stack;
    }

    @Override
    public void getSubItems(@Nonnull CreativeTabs tab, @Nonnull NonNullList<ItemStack> items) {
        if (!this.isInCreativeTab(tab)) {
            return;
        }
        items.add(getFrame(100));
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn,
        EnumHand handIn) {
        ItemStack heldStack = playerIn.getHeldItem(handIn);
        if (!worldIn.isRemote) {
            playerIn.renderBrokenItemStack(heldStack);
        }
        heldStack.shrink(1);
        return ActionResult.newResult(EnumActionResult.SUCCESS, heldStack);
    }
}
