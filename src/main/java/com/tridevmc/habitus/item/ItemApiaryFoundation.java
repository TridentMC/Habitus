/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.item;

import com.tridevmc.habitus.init.HSItems;
import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.NonNullList;
import net.minecraft.world.World;

public class ItemApiaryFoundation extends ItemBase {

    public ItemApiaryFoundation() {
        super("apiary_foundation");
    }

    public NBTTagCompound serializeToNBT(ItemStack stack) {
        if (stack == ItemStack.EMPTY || !(stack.getItem() instanceof ItemApiaryFoundation)) {
            return null;
        }

        NBTTagCompound compound = stack.getTagCompound();
        if (compound == null) {
            compound = new NBTTagCompound();
            compound.setInteger("size", 0);
        }
        return compound;
    }

    @Override
    public boolean getHasSubtypes() {
        return true;
    }

    private static ItemStack getFoundation(int size) {
        ItemStack stack = new ItemStack(HSItems.apiaryFoundation, 1);
        NBTTagCompound compound = new NBTTagCompound();
        compound.setInteger("size", size);
        stack.setTagCompound(compound);
        return stack;
    }

    @Override
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip,
        ITooltipFlag flagIn) {
        NBTTagCompound compound = stack.getTagCompound();
        if (compound != null) {
            int size = compound.getInteger("size");
            tooltip.add("Size: " + size);
        }
    }

    @Override
    public void getSubItems(@Nonnull CreativeTabs tab, @Nonnull NonNullList<ItemStack> items) {
        if (!this.isInCreativeTab(tab)) {
            return;
        }
        items.add(getFoundation(10));
        items.add(getFoundation(12));
        items.add(getFoundation(14));
        items.add(getFoundation(16));
        items.add(getFoundation(18));
    }
}
