/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.init;

import com.tridevmc.habitus.block.*;
import com.tridevmc.habitus.generic.IOreDict;
import java.util.Objects;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.registries.IForgeRegistry;

@SuppressWarnings("WeakerAccess")
public class HSBlocks {

    public static final BlockHive hive = new BlockHive();

    public static final BlockLogBase cherryLog = new BlockLogBase("cherry");
    public static final BlockLogBase whiteCherryLog = new BlockLogBase("cherry_white");

    public static final BlockSaplingCherry cherrySapling = new BlockSaplingCherry();
    public static final BlockSaplingWhiteCherry whiteCherrySapling = new BlockSaplingWhiteCherry();

    public static final BlockLeavesBase cherryLeaves = new BlockLeavesBase("cherry");
    public static final BlockLeavesBase whiteCherryLeaves = new BlockLeavesBase("cherry_white");

    public static final BlockPlanksBase cherryPlanks = new BlockPlanksBase("cherry");
    public static final BlockPlanksBase whiteCherryPlanks = new BlockPlanksBase("cherry_white");
    public static final BlockPlanksBase treatedPlanks = new BlockPlanksBase("treated")
        .setShouldOreDict(false);

    public static final BlockFPPlanksVanilla fpPlanksVanilla = new BlockFPPlanksVanilla();

    public static final BlockSlabCherry.Half cherrySlab = new BlockSlabCherry.Half();
    public static final BlockSlabCherry.Double doubleCherrySlab = new BlockSlabCherry.Double();
    public static final BlockSlabWhiteCherry.Half whiteCherrySlab = new BlockSlabWhiteCherry.Half();
    public static final BlockSlabWhiteCherry.Double doubleWhiteCherrySlab = new BlockSlabWhiteCherry.Double();
    public static final BlockSlabTreated.Half treatedSlab = new BlockSlabTreated.Half();
    public static final BlockSlabTreated.Double doubleTreatedSlab = new BlockSlabTreated.Double();

    public static final BlockFenceBase cherryFence = new BlockFenceBase(Material.WOOD, MapColor.PINK, "cherry");
    public static final BlockFenceBase whiteCherryFence = new BlockFenceBase(Material.WOOD, MapColor.WHITE_STAINED_HARDENED_CLAY, "cherry_white");
    public static final BlockFenceGateBase cherryFenceGate = new BlockFenceGateBase(MapColor.PINK, "cherry");
    public static final BlockFenceGateBase whiteCherryFenceGate = new BlockFenceGateBase(MapColor.WHITE_STAINED_HARDENED_CLAY, "cherry_white");

    public static final BlockStairsWood cherryStairs = new BlockStairsWood(
        cherryPlanks.getDefaultState(), "cherry");
    public static final BlockStairsWood whiteCherryStairs = new BlockStairsWood(
            whiteCherryPlanks.getDefaultState(), "cherry_white");
    public static final BlockStairsWood treatedStairs = new BlockStairsWood(
        treatedPlanks.getDefaultState(), "treated").setShouldOreDict(false);

    public static final BlockDoorBase cherryDoor = new BlockDoorBase(Material.WOOD, "cherry")
        .setHardness(3.0F).setSoundType(SoundType.WOOD);

    public static final BlockDoorBase whiteCherryDoor = new BlockDoorBase(Material.WOOD, "cherry_white")
            .setHardness(3.0F).setSoundType(SoundType.WOOD);

    public static final BlockApiary apiary = new BlockApiary();
    public static final BlockBeeAnalyzer beeAnalyzer = new BlockBeeAnalyzer();

    public static final BlockBarley barley = new BlockBarley();

    public static final BlockOre copperOre = new BlockOre("copper_ore");
    public static final BlockOre tinOre = new BlockOre("tin_ore");
    public static final BlockBase bronzeBlock = new BlockBase(Material.IRON, "bronze_block")
            .setSoundType(SoundType.METAL)
            .setHardness(5.0F)
            .setResistance(10.0F);

    public static final BlockBarleyBale barleyBale = new BlockBarleyBale();

    public static final BlockFluidHoney honey = new BlockFluidHoney();

    private static void registerBlock(IForgeRegistry<Block> registry, IHabitusBlock block) {
        registry.register(((Block) block));

        if (block instanceof BlockTEBase) {
            GameRegistry.registerTileEntity(((BlockTEBase<?>) block).getTileEntityClass(),
                Objects.requireNonNull(((Block) block).getRegistryName()).toString());
        }

    }

    public static void registerItemBlock(IForgeRegistry<Item> registry, ItemBlock itemBlock) {
        registry.register(itemBlock);

        if (itemBlock.getBlock() instanceof IOreDict) {
            ((IOreDict) itemBlock.getBlock()).registerOreDict();
        }
    }

    public static void registerBlockModel(IHabitusBlock block) {
        block.registerItemModel((ItemBlock) Item.getItemFromBlock((Block) block));
    }

    public static void registerBlocks(IForgeRegistry<Block> registry) {
        registerBlock(registry, hive);
        registerBlock(registry, cherryLog);
        registerBlock(registry, cherryPlanks);
        registerBlock(registry, cherryLeaves);
        registerBlock(registry, whiteCherryLog);
        registerBlock(registry, whiteCherryPlanks);
        registerBlock(registry, whiteCherryLeaves);
        registerBlock(registry, cherrySapling);
        registerBlock(registry, whiteCherrySapling);
        registerBlock(registry, cherrySlab);
        registerBlock(registry, doubleCherrySlab);
        registerBlock(registry, whiteCherrySlab);
        registerBlock(registry, doubleWhiteCherrySlab);
        registerBlock(registry, cherryStairs);
        registerBlock(registry, whiteCherryStairs);
        registerBlock(registry, cherryDoor);
        registerBlock(registry, whiteCherryDoor);
        registerBlock(registry, treatedPlanks);
        registerBlock(registry, treatedSlab);
        registerBlock(registry, doubleTreatedSlab);
        registerBlock(registry, treatedStairs);
        registerBlock(registry, cherryFence);
        registerBlock(registry, whiteCherryFence);
        registerBlock(registry, cherryFenceGate);
        registerBlock(registry, whiteCherryFenceGate);
        registerBlock(registry, fpPlanksVanilla);
        registerBlock(registry, apiary);
        registerBlock(registry, beeAnalyzer);
        registerBlock(registry, barley);
        registerBlock(registry, copperOre);
        registerBlock(registry, tinOre);
        registerBlock(registry, bronzeBlock);
        registerBlock(registry, barleyBale);
        registerBlock(registry, honey);
    }

    public static void registerItemBlocks(IForgeRegistry<Item> registry) {
        registerItemBlock(registry, hive.getItemBlock());
        registerItemBlock(registry, cherryLog.getItemBlock());
        registerItemBlock(registry, cherryPlanks.getItemBlock());
        registerItemBlock(registry, cherryLeaves.getItemBlock());
        registerItemBlock(registry, cherrySapling.getItemBlock());
        registerItemBlock(registry, whiteCherryLog.getItemBlock());
        registerItemBlock(registry, whiteCherryPlanks.getItemBlock());
        registerItemBlock(registry, whiteCherryLeaves.getItemBlock());
        registerItemBlock(registry, whiteCherrySapling.getItemBlock());
        cherryLeaves.setSapling(Item.getItemFromBlock(cherrySapling));
        whiteCherryLeaves.setSapling(Item.getItemFromBlock(whiteCherrySapling));
        registerItemBlock(registry, cherrySlab.getItemBlock());
        registerItemBlock(registry, whiteCherrySlab.getItemBlock());
        registerItemBlock(registry, cherryStairs.getItemBlock());
        registerItemBlock(registry, whiteCherryStairs.getItemBlock());
        registerItemBlock(registry, treatedPlanks.getItemBlock());
        registerItemBlock(registry, treatedSlab.getItemBlock());
        registerItemBlock(registry, treatedStairs.getItemBlock());
        registerItemBlock(registry, cherryFence.getItemBlock());
        registerItemBlock(registry, whiteCherryFence.getItemBlock());
        registerItemBlock(registry, cherryFenceGate.getItemBlock());
        registerItemBlock(registry, whiteCherryFenceGate.getItemBlock());
        registerItemBlock(registry, fpPlanksVanilla.getItemBlock());
        registerItemBlock(registry, apiary.getItemBlock());
        registerItemBlock(registry, beeAnalyzer.getItemBlock());
        registerItemBlock(registry, copperOre.getItemBlock());
        registerItemBlock(registry, tinOre.getItemBlock());
        registerItemBlock(registry, bronzeBlock.getItemBlock());
        registerItemBlock(registry, barleyBale.getItemBlock());
        registerItemBlock(registry, honey.getItemBlock());
    }

    public static void registerBlockModels() {
        registerBlockModel(hive);
        registerBlockModel(cherryLog);
        registerBlockModel(cherryPlanks);
        registerBlockModel(cherryLeaves);
        registerBlockModel(cherrySapling);
        registerBlockModel(whiteCherryLog);
        registerBlockModel(whiteCherryPlanks);
        registerBlockModel(whiteCherryLeaves);
        registerBlockModel(whiteCherrySapling);
        registerBlockModel(cherrySlab);
        registerBlockModel(whiteCherrySlab);
        registerBlockModel(cherryStairs);
        registerBlockModel(whiteCherryStairs);
        registerBlockModel(treatedPlanks);
        registerBlockModel(treatedSlab);
        registerBlockModel(treatedStairs);
        registerBlockModel(cherryFence);
        registerBlockModel(whiteCherryFence);
        registerBlockModel(cherryFenceGate);
        registerBlockModel(whiteCherryFenceGate);
        registerBlockModel(fpPlanksVanilla);
        registerBlockModel(apiary);
        registerBlockModel(beeAnalyzer);
        registerBlockModel(copperOre);
        registerBlockModel(tinOre);
        registerBlockModel(bronzeBlock);
        registerBlockModel(barleyBale);
        registerBlockModel(honey);
    }
}
