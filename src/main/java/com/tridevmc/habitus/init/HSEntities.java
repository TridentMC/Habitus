/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.init;

import com.tridevmc.habitus.entity.*;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.structure.MapGenNetherBridge;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.event.terraingen.InitMapGenEvent;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.registry.EntityEntry;
import net.minecraftforge.fml.common.registry.EntityEntryBuilder;
import net.minecraftforge.registries.IForgeRegistry;

public class HSEntities {

    private static int ENTITY_ID = 0;

    public static void registerEntities(IForgeRegistry<EntityEntry> registry) {

        registry.register(EntityEntryBuilder.create()
            .entity(EntityBeeSwarm.EntityTemperateBeeSwarm.class)
            .id(new ResourceLocation("habitus", "temperate_bee_swarm"), ENTITY_ID++)
            .name("habitus.temperate_bee_swarm")
            .egg(0x6bed47, 0x1b6307)
            .tracker(80, 3, true)
            .spawn(EnumCreatureType.CREATURE, 16, 1, 1,
                BiomeTools.BiomeListBuilder.create()
                    .andIs(BiomeDictionary.Type.FOREST)
                    .andIs(BiomeDictionary.Type.PLAINS)
                    .andIs(BiomeDictionary.Type.HILLS)
                    .andIs(BiomeDictionary.Type.CONIFEROUS)
                    .andIsNot(BiomeDictionary.Type.MOUNTAIN)
                    .andIsNot(BiomeDictionary.Type.HOT)
                    .andIsNot(BiomeDictionary.Type.COLD)
                    .build()).build());

        registry.register(EntityEntryBuilder.create()
            .entity(EntityBeeSwarm.EntityFrigidBeeSwarm.class)
            .id(new ResourceLocation("habitus", "frigid_bee_swarm"), ENTITY_ID++)
            .name("habitus.frigid_bee_swarm")
            .egg(0x6aeddf, 0x0da897)
            .tracker(80, 3, true)
            .spawn(EnumCreatureType.CREATURE, 16, 1, 1,
                BiomeTools.BiomeListBuilder.create()
                    .andIs(BiomeDictionary.Type.COLD)
                    .andIs(BiomeDictionary.Type.MOUNTAIN)
                    .andIsNot(BiomeDictionary.Type.END)
                    .andIsNot(BiomeDictionary.Type.RIVER)
                    .andIsNot(BiomeDictionary.Type.OCEAN)
                    .andIsNot(BiomeDictionary.Type.BEACH)
                    .build()).build());

        registry.register(EntityEntryBuilder.create()
            .entity(EntityBeeSwarm.EntityAridBeeSwarm.class)
            .id(new ResourceLocation("habitus", "arid_bee_swarm"), ENTITY_ID++)
            .name("habitus.arid_bee_swarm")
            .egg(0xffe175, 0x6d5705)
            .tracker(80, 3, true)
            .spawn(EnumCreatureType.CREATURE, 16, 1, 1,
                BiomeTools.BiomeListBuilder.create()
                    .andIs(BiomeDictionary.Type.HOT)
                    .andIs(BiomeDictionary.Type.DRY)
                    .andIsNot(BiomeDictionary.Type.NETHER)
                    .andIsNot(BiomeDictionary.Type.MESA)
                    .andIsNot(BiomeDictionary.Type.COLD)
                    .andIsNot(BiomeDictionary.Type.WET)
                    .andIsNot(BiomeDictionary.Type.RIVER)
                    .andIsNot(BiomeDictionary.Type.OCEAN)
                    .andIsNot(BiomeDictionary.Type.BEACH)
                    .build()).build());

        registry.register(EntityEntryBuilder.create()
            .entity(EntityBeeSwarm.EntitySwampyBeeSwarm.class)
            .id(new ResourceLocation("habitus", "swampy_bee_swarm"), ENTITY_ID++)
            .name("habitus.swampy_bee_swarm")
            .egg(0x0c3311, 0x306036)
            .tracker(80, 3, true)
            .spawn(EnumCreatureType.CREATURE, 16, 1, 1,
                BiomeTools.BiomeListBuilder.create()
                    .andIs(BiomeDictionary.Type.SWAMP)
                    .andIs(BiomeDictionary.Type.WET)
                    .andIsNot(BiomeDictionary.Type.JUNGLE)
                    .andIsNot(BiomeDictionary.Type.DRY)
                    .andIsNot(BiomeDictionary.Type.RIVER)
                    .andIsNot(BiomeDictionary.Type.OCEAN)
                    .andIsNot(BiomeDictionary.Type.BEACH)
                    .build()).build());

        registry.register(EntityEntryBuilder.create()
            .entity(EntityBeeSwarm.EntityHellishBeeSwarm.class)
            .id(new ResourceLocation("habitus", "hellish_bee_swarm"), ENTITY_ID++)
            .name("habitus.hellish_bee_swarm")
            .egg(0x4f0303, 0xed1e1e)
            .tracker(80, 3, true)
            .spawn(EnumCreatureType.CREATURE, 16, 1, 1,
                BiomeTools.BiomeListBuilder.create()
                    .andIs(BiomeDictionary.Type.NETHER)
                    .build()).build());

        registry.register(EntityEntryBuilder.create()
            .entity(EntityBeeSwarm.EntityFungalBeeSwarm.class)
            .id(new ResourceLocation("habitus", "fungal_bee_swarm"), ENTITY_ID++)
            .name("habitus.fungal_bee_swarm")
            .egg(0x3f280f, 0x664a2b)
            .tracker(80, 3, true)
            .spawn(EnumCreatureType.CREATURE, 16, 1, 1,
                BiomeTools.BiomeListBuilder.create()
                    .andIs(BiomeDictionary.Type.MUSHROOM)
                    .build()).build());

        registry.register(EntityEntryBuilder.create()
            .entity(EntityBeeSwarm.EntityCeramicBeeSwarm.class)
            .id(new ResourceLocation("habitus", "ceramic_bee_swarm"), ENTITY_ID++)
            .name("habitus.ceramic_bee_swarm")
            .egg(0x844d36, 0xad806d)
            .tracker(80, 3, true)
            .spawn(EnumCreatureType.CREATURE, 16, 1, 1,
                BiomeTools.BiomeListBuilder.create()
                    .andIs(BiomeDictionary.Type.MESA)
                    .build()).build());

        registry.register(EntityEntryBuilder.create()
            .entity(EntityBeeSwarm.EntityTranscendentBeeSwarm.class)
            .id(new ResourceLocation("habitus", "transcendent_bee_swarm"), ENTITY_ID++)
            .name("habitus.transcendent_bee_swarm")
            .egg(0x490c63, 0x9b59b7)
            .tracker(80, 3, true)
            .spawn(EnumCreatureType.CREATURE, 16, 1, 1,
                BiomeTools.BiomeListBuilder.create()
                    .andIs(BiomeDictionary.Type.END)
                    .build()).build());

        registry.register(EntityEntryBuilder.create()
            .entity(EntityBeeSwarm.EntityHumidBeeSwarm.class)
            .id(new ResourceLocation("habitus", "humid_bee_swarm"), ENTITY_ID++)
            .name("habitus.humid_bee_swarm")
            .egg(0x093000, 0x030c00)
            .tracker(80, 3, true)
            .spawn(EnumCreatureType.CREATURE, 16, 1, 1,
                BiomeTools.BiomeListBuilder.create()
                    .andIs(BiomeDictionary.Type.JUNGLE)
                    .build()).build());

        registry.register(EntityEntryBuilder.create()
                .entity(EntityBeeSwarm.EntityWitheredBeeSwarm.class)
                .id(new ResourceLocation("habitus", "withered_bee_Swarm"), ENTITY_ID++)
                .name("habitus.withered_bee_swarm")
                .egg(0x34373a, 0x4c4f51)
                .tracker(80, 3, true).build());

        registry.register(EntityEntryBuilder.create()
                .entity(EntityDroneSwarm.class)
                .id(new ResourceLocation("habitus", "drone_swarm"), ENTITY_ID++)
                .name("habitus.drone_swarm")
                .egg(0x4286f4, 0x167ea0)
                .tracker(160, 3, true).build());

        registry.register(EntityEntryBuilder.create()
                .entity(EntityBeeGrenade.class)
                .id(new ResourceLocation("habitus",  "bee_grenade"), ENTITY_ID++)
                .name("habitus.bee_grenade")
                .tracker(64, 10, true).build());
    }

    public static void onInitMapGen(InitMapGenEvent event) {
        if(event.getType() == InitMapGenEvent.EventType.NETHER_BRIDGE) {
            MapGenNetherBridge gen = (MapGenNetherBridge)event.getNewGen();
            gen.getSpawnList().add(new Biome.SpawnListEntry(EntityBeeSwarm.EntityWitheredBeeSwarm.class, 16, 1, 1));
        }
    }

    public static void registerEntityRenderer() {
        RenderingRegistry
            .registerEntityRenderingHandler(EntityBeeSwarm.EntityTemperateBeeSwarm.class,
                RenderEntityBeeSwarm::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityBeeSwarm.EntityFrigidBeeSwarm.class,
            RenderEntityBeeSwarm::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityBeeSwarm.EntityAridBeeSwarm.class,
            RenderEntityBeeSwarm::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityBeeSwarm.EntitySwampyBeeSwarm.class,
            RenderEntityBeeSwarm::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityBeeSwarm.EntityHellishBeeSwarm.class,
            RenderEntityBeeSwarm::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityBeeSwarm.EntityFungalBeeSwarm.class,
            RenderEntityBeeSwarm::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityBeeSwarm.EntityCeramicBeeSwarm.class,
            RenderEntityBeeSwarm::new);
        RenderingRegistry
            .registerEntityRenderingHandler(EntityBeeSwarm.EntityTranscendentBeeSwarm.class,
                RenderEntityBeeSwarm::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityBeeSwarm.EntityHumidBeeSwarm.class,
            RenderEntityBeeSwarm::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityBeeSwarm.EntityWitheredBeeSwarm.class,
                RenderEntityBeeSwarm::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityDroneSwarm.class,
                RenderEntityBeeSwarm::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityBeeGrenade.class,
              new RenderSnowballFactory());
    }
}
