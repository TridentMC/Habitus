/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.init;

import com.tridevmc.habitus.Habitus;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.registries.IForgeRegistry;

public class HSSounds {

    public static final SoundEvent bee_swarm_idle = createSoundEvent("bee_swarm_idle");
    public static final SoundEvent vacuum_use = createSoundEvent("vacuum_use");

    private static SoundEvent createSoundEvent(String resourceLocation) {
        ResourceLocation sound = new ResourceLocation(Habitus.MOD_ID, resourceLocation);
        return new SoundEvent(sound).setRegistryName(sound);
    }

    public static void registerSounds(IForgeRegistry<SoundEvent> registry) {
        registry.register(bee_swarm_idle);
        registry.register(vacuum_use);
    }
}
