/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.init;

import com.tridevmc.habitus.Habitus;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.Fluid;

@SuppressWarnings("EmptyMethod")
public class HSFluids {

    public static final Fluid fluid_honey = new Fluid(Habitus.MOD_ID + ".honey",
        new ResourceLocation("habitus:blocks/honey_still"),
        new ResourceLocation("habitus:blocks/honey_flow"))
        .setLuminosity(0).setDensity(1420).setViscosity(10000).setTemperature(300);

    public static void init() {
        net.minecraftforge.fluids.FluidRegistry.registerFluid(fluid_honey);
        net.minecraftforge.fluids.FluidRegistry.addBucketForFluid(fluid_honey);
    }
}
