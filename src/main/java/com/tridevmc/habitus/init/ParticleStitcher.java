/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.init;

import com.tridevmc.habitus.particle.ParticleBee;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Mod.EventBusSubscriber
class ParticleStitcher {

    @SubscribeEvent
    public static void stitcherEventPre(TextureStitchEvent.Pre event) {
        event.getMap().registerSprite(ParticleBee.beeTexture);
        event.getMap().registerSprite(ParticleBee.frigidBeeTexture);
        event.getMap().registerSprite(ParticleBee.aridBeeTexture);
        event.getMap().registerSprite(ParticleBee.swampyBeeTexture);
        event.getMap().registerSprite(ParticleBee.hellishBeeTexture);
        event.getMap().registerSprite(ParticleBee.fungalBeeTexture);
        event.getMap().registerSprite(ParticleBee.ceramicBeeTexture);
        event.getMap().registerSprite(ParticleBee.transcendentBeeTexture);
        event.getMap().registerSprite(ParticleBee.humidBeeTexture);
        event.getMap().registerSprite(new ResourceLocation("habitus:items/empty_frame_slot"));
    }
}
