/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.init;

import com.elytradev.concrete.inventory.gui.ConcreteContainer;
import com.tridevmc.habitus.gui.GuiBase;
import com.tridevmc.habitus.gui.ILockableInventory;
import com.tridevmc.habitus.gui.container.ContainerApiary;
import com.tridevmc.habitus.gui.container.ContainerBeeAnalyzer;
import com.tridevmc.habitus.tile.TileEntityApiary;
import com.tridevmc.habitus.tile.TileEntityBeeAnalyzer;
import javax.annotation.Nullable;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class GuiHandler implements IGuiHandler {

    public static final int GUI_APIARY = 0;
    public static final int GUI_BEE_ANALYZER = 1;

    @Nullable
    @Override
    public Container getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y,
        int z) {
        ConcreteContainer container = null;
        TileEntity te = world.getTileEntity(new BlockPos(x, y, z));
        if (te != null) {
            switch (ID) {
                case GUI_APIARY: {
                    container = new ContainerApiary((TileEntityApiary) te, player.inventory,
                            (ILockableInventory)((TileEntityApiary) te).getContainerInventory());
                    break;
                }
                case GUI_BEE_ANALYZER: {
                    container = new ContainerBeeAnalyzer((TileEntityBeeAnalyzer) te,
                        player.inventory, ((TileEntityBeeAnalyzer) te).getContainerInventory());
                    break;
                }
            }

            if (container != null) {
                container.validate();
            }
        }

        return container;
    }

    @Nullable
    @Override
    @SideOnly(Side.CLIENT)
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y,
        int z) {
        switch (ID) {
            case GUI_APIARY:
            case GUI_BEE_ANALYZER:
                return new GuiBase(
                    (ConcreteContainer) getServerGuiElement(ID, player, world, x, y, z));
        }
        return null;
    }
}
