/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.init;

import com.tridevmc.habitus.generic.IOreDict;
import com.tridevmc.habitus.item.*;
import net.minecraft.item.Item;
import net.minecraftforge.registries.IForgeRegistry;

@SuppressWarnings("WeakerAccess")
public class HSItems {

    public static final ItemBeeStick beeStick = new ItemBeeStick();
    public static final ItemApiaryFrame apiaryFrame = new ItemApiaryFrame();
    public static final ItemApiaryFoundation apiaryFoundation = new ItemApiaryFoundation();
    public static final ItemAssembledApiaryFrame assembledApiaryFrame = new ItemAssembledApiaryFrame();
    public static final ItemBeeVacuum beeVacuum = new ItemBeeVacuum();
    public static final ItemCreosoteOil creosoteOil = new ItemCreosoteOil();
    public static final ItemBarleySeeds barleySeeds = new ItemBarleySeeds();
    public static final ItemBase barley = new ItemBase("barley");
    public static final ItemDoorCherry cherryDoor = new ItemDoorCherry();
    public static final ItemDoorWhiteCherry whiteCherryDoor = new ItemDoorWhiteCherry();
    public static final ItemBase helletonDust = new ItemBase("helleton_dust");
    public static final ItemBase fireproofingOil = new ItemBase("fireproofing_oil");
    public static final ItemIngot copperIngot = new ItemIngot("copper_ingot", "ingotCopper");
    public static final ItemIngot tinIngot = new ItemIngot("tin_ingot", "ingotTin");
    public static final ItemIngot bronzeIngot = new ItemIngot("bronze_ingot", "ingotBronze");
    public static final ItemBase ledRed = new ItemBase("led_red");
    public static final ItemBase ledGreen = new ItemBase("led_green");
    public static final ItemBase ledBlue = new ItemBase("led_blue");
    public static final ItemBase screen = new ItemBase("screen");
    public static final ItemBalancedApiaryFrame balancedApiaryFrame = new ItemBalancedApiaryFrame();
    public static final ItemAssembledBalancedApiaryFrame assembledBalancedApiaryFrame = new ItemAssembledBalancedApiaryFrame();
    public static final ItemBeeGrenade beeGrenade = new ItemBeeGrenade("bee_grenade");

    public static void registerItems(IForgeRegistry<Item> registry) {
        registerItem(registry, beeStick);
        registerItem(registry, apiaryFrame);
        registerItem(registry, apiaryFoundation);
        registerItem(registry, assembledApiaryFrame);
        registerItem(registry, beeVacuum);
        registerItem(registry, creosoteOil);
        registerItem(registry, barleySeeds);
        registerItem(registry, barley);
        registerItem(registry, cherryDoor);
        registerItem(registry, whiteCherryDoor);
        registerItem(registry, helletonDust);
        registerItem(registry, fireproofingOil);
        registerItem(registry, copperIngot);
        registerItem(registry, tinIngot);
        registerItem(registry, bronzeIngot);
        registerItem(registry, ledRed);
        registerItem(registry, ledGreen);
        registerItem(registry, ledBlue);
        registerItem(registry, screen);
        registerItem(registry, balancedApiaryFrame);
        registerItem(registry, assembledBalancedApiaryFrame);
        registerItem(registry, beeGrenade);
    }

    public static void registerItemModels() {
        registerItemModel(beeStick);
        registerItemModel(apiaryFrame);
        registerItemModel(apiaryFoundation);
        registerItemModel(assembledApiaryFrame);
        registerItemModel(beeVacuum);
        registerItemModel(creosoteOil);
        registerItemModel(barleySeeds);
        registerItemModel(barley);
        registerItemModel(cherryDoor);
        registerItemModel(whiteCherryDoor);
        registerItemModel(helletonDust);
        registerItemModel(fireproofingOil);
        registerItemModel(copperIngot);
        registerItemModel(tinIngot);
        registerItemModel(bronzeIngot);
        registerItemModel(ledRed);
        registerItemModel(ledGreen);
        registerItemModel(ledBlue);
        registerItemModel(screen);
        registerItemModel(balancedApiaryFrame);
        registerItemModel(assembledBalancedApiaryFrame);
        registerItemModel(beeGrenade);
    }

    private static void registerItem(IForgeRegistry<Item> registry, Item item) {
        registry.register(item);

        if (item instanceof IOreDict) {
            ((IOreDict) item).registerOreDict();
        }
    }

    private static void registerItemModel(IHabitusItem item) {
        item.registerItemModel();
    }
}
