/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.world;

import com.tridevmc.habitus.entity.BiomeTools;
import com.tridevmc.habitus.init.HSBlocks;
import java.util.Random;
import java.util.Set;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraft.world.gen.feature.WorldGenTrees;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.fml.common.IWorldGenerator;

public class WorldGen implements IWorldGenerator {

    private final static Set<Biome> cherryList = BiomeTools.BiomeListBuilder.create()
        .andIs(BiomeDictionary.Type.FOREST)
        .andIsNot(BiomeDictionary.Type.CONIFEROUS)
        .andIsNot(BiomeDictionary.Type.COLD)
        .andIsNot(BiomeDictionary.Type.HOT).build();

    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world,
        IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        WorldGenTrees cherry = new WorldGenTrees(true, 5, HSBlocks.cherryLog.getDefaultState(),
            HSBlocks.cherryLeaves.getDefaultState(), false);
        WorldGenTrees whiteCherry = new WorldGenTrees(true, 5, HSBlocks.whiteCherryLog.getDefaultState(),
                HSBlocks.whiteCherryLeaves.getDefaultState(), false);
        for (int i = 0; i < 4; i++) {
            int X = (chunkX * 16) + random.nextInt(16) + 8;
            int Z = (chunkZ * 16) + random.nextInt(16) + 8;
            int Y = world.getHeight(X, Z);
            BlockPos pos = new BlockPos(X, Y, Z);
            if (cherryList.contains(world.getBiome(pos))) {
                if(random.nextBoolean())
                    cherry.generate(world, random, new BlockPos(X, Y, Z));
                else
                    whiteCherry.generate(world, random, new BlockPos(X, Y, Z));
            }
        }
    }

    private void generateOre(World world, Random rand, int chunkX, int chunkZ,
        IBlockState blockState, int chances, int yMin, int yMax, int veinSize) {
        int range = yMax - yMin;
        for (int i = 0; i < chances; i++) {
            BlockPos pos = new BlockPos((chunkX * 16) + rand.nextInt(16),
                yMin + rand.nextInt(range), (chunkZ * 16) + rand.nextInt(16));
            WorldGenMinable oreGen = new WorldGenMinable(blockState, veinSize);
            oreGen.generate(world, rand, pos);
        }
    }

}
