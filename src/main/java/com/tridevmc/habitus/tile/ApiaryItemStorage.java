package com.tridevmc.habitus.tile;

import com.elytradev.concrete.inventory.ConcreteItemStorage;
import net.minecraft.item.ItemStack;

import javax.annotation.Nonnull;

public class ApiaryItemStorage extends ConcreteItemStorage {

    private boolean framesLocked = false;

    public ApiaryItemStorage() {
        super(3);
    }

    public void setFramesLocked(boolean locked) {
        framesLocked = locked;
    }

    public boolean framesLocked() {
        return this.framesLocked;
    }

    @Nonnull
    @Override
    public ItemStack extractItem(int slot, int amount, boolean simulate) {
        if(!framesLocked)
            return super.extractItem(slot, amount, simulate);
        return ItemStack.EMPTY;
    }

    @Override
    public boolean getCanExtract(int slot) {
        return !framesLocked && super.getCanExtract(slot);
    }




}
