/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.tile;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ITickable;
import net.minecraft.world.IInteractionObject;
import net.minecraft.world.IWorldNameable;

@SuppressWarnings("EmptyMethod")
public abstract class TileEntityBase extends TileEntity implements IInteractionObject,
    IWorldNameable, ITEUsableStrictness, ITickable, ITEFieldable, ITEActivatable {

    @Override
    public boolean isUsableByPlayer(EntityPlayer player) {
        return this.world == null || this.world.getTileEntity(this.pos) == this && player
            .getDistanceSq(this.pos.getX() + 0.5D, this.pos.getY() + 0.5D, this.pos.getZ() + 0.5D)
            <= 64D;

    }


    public abstract void writeItemData(NBTTagCompound compound);

    public abstract void readItemData(NBTTagCompound compound);
}