/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.tile;

import com.tridevmc.habitus.entity.BeePhenotype;
import com.tridevmc.habitus.entity.EnumBeeBreed;
import com.tridevmc.habitus.entity.EnumBeeHealth;
import com.tridevmc.habitus.entity.EnumBeeTemperament;
import java.util.Random;
import net.minecraft.nbt.NBTTagCompound;

public class BeePopulation {

    private static final int TIME_TO_UPDATE = 100;
    private static final int TIME_TO_WEATHER_UPDATE = 20;
    public static final int MAXIMUM_POPULATION = 80000;
    private static final int MINIMUM_POPULATION_SWARM = 20000;

    private int timeToNextUpdate = TIME_TO_UPDATE;
    private int timeToNextWeatherUpdate = TIME_TO_WEATHER_UPDATE;

    private final Random rand;
    private int populationCount = 0;
    private EnumBeeTemperament temperament;
    private EnumBeeBreed breed;
    private EnumBeeHealth health;
    private BeePhenotype phenotype;

    private float growthFactor = 1.0f;
    private float deathFactor = 1.0f;

    private int growthQuanta = 100;
    private int deathQuanta = 85;

    public BeePopulation() {
        rand = new Random();
        temperament = EnumBeeTemperament.NEUTRAL;
        breed = EnumBeeBreed.TEMPERATE;
        health = EnumBeeHealth.HEALTHY;
        phenotype = new BeePhenotype();
    }

    public BeePopulation copy() {
        BeePopulation copy = new BeePopulation();
        copy.setPopulationCount(this.populationCount);
        copy.setTemperament(this.temperament);
        copy.setBreed(this.breed);
        copy.setPhenotype(this.phenotype);
        return copy;
    }

    @Override
    public String toString() {
        return "PopulationCount: " + populationCount + " - Temperament: " + temperament.getName()
            + " - Breed " + breed.getName();
    }

    public static BeePopulation generateRandomPopulation() {
        BeePopulation population = new BeePopulation();
        population.setPopulationCount(
            population.rand.nextInt(MAXIMUM_POPULATION - MINIMUM_POPULATION_SWARM)
                + MINIMUM_POPULATION_SWARM);
        population.temperament = EnumBeeTemperament.fromInt(population.rand.nextInt(5));
        population.breed = EnumBeeBreed.fromInt(population.rand.nextInt(EnumBeeBreed.BREED_COUNT));
        return population;
    }

    public void giveRandomHivePhenotype() {
        phenotype = new BeePhenotype();
        int nS = 0, nR = 0, nI = 0;
        for (EnumBeeHealth h : EnumBeeHealth.values()) {
            if (h == EnumBeeHealth.HEALTHY) {
                continue;
            }

            if (nS < 1 && rand.nextInt(100) < 10) {
                phenotype.giveSusceptibility(h);
                nS++;
                continue;
            }

            if (nR < 3 && rand.nextInt(100) < 30) {
                phenotype.giveResistance(h);
                nR++;
                continue;
            }

            if (nI < 2 && rand.nextInt(100) < 15) {
                phenotype.giveImmunity(h);
                nI++;
            }
        }
    }

    public void giveRandomSwarmPhenotype() {
        phenotype = new BeePhenotype();
        int nS = 0, nR = 0, nI = 0;
        for (EnumBeeHealth h : EnumBeeHealth.values()) {
            if (h == EnumBeeHealth.HEALTHY) {
                continue;
            }

            if (nS < 2 && rand.nextInt(100) < 25) {
                phenotype.giveSusceptibility(h);
                nS++;
                continue;
            }

            if (nR < 2 && rand.nextInt(100) < 15) {
                phenotype.giveResistance(h);
                nR++;
                continue;
            }

            if (nI < 1 && rand.nextInt(100) < 5) {
                phenotype.giveImmunity(h);
                nI++;
            }
        }
    }

    public EnumBeeHealth getHealth() {
        return health;
    }

    public void writeToNBT(NBTTagCompound compound) {
        NBTTagCompound populationCompound = new NBTTagCompound();
        populationCompound.setInteger("population_count", populationCount);
        populationCompound.setInteger("temperament", temperament.getId());
        populationCompound.setInteger("breed", breed.getId());
        populationCompound.setInteger("health", health.getId());
        populationCompound.setTag("phenotype", phenotype.serializeToNBT());
        compound.setTag("bee_population", populationCompound);
    }

    public void readFromNBT(NBTTagCompound compound) {
        NBTTagCompound populationCompound = compound.getCompoundTag("bee_population");
        populationCount = populationCompound.getInteger("population_count");
        temperament = EnumBeeTemperament.fromInt(populationCompound.getInteger("temperament"));
        breed = EnumBeeBreed.fromInt(populationCompound.getInteger("breed"));
        phenotype = new BeePhenotype();
        phenotype.deserializeFromNBT(populationCompound.getCompoundTag("phenotype"));
    }

    public NBTTagCompound serializeToNBT() {
        NBTTagCompound compound = new NBTTagCompound();
        writeToNBT(compound);
        return compound;
    }

    public int getPopulationCount() {
        return populationCount;
    }

    public void setPopulationCount(int count) {
        populationCount = count;
    }

    public EnumBeeTemperament getTemperament() {
        return temperament;
    }

    public void setTemperament(EnumBeeTemperament temperament) {
        this.temperament = temperament;
    }

    public EnumBeeBreed getBreed() {
        return breed;
    }

    public void setBreed(EnumBeeBreed breed) {
        this.breed = breed;
    }

    public BeePhenotype getPhenotype() {
        return phenotype;
    }

    public void setPhenotype(BeePhenotype phenotype) {
        this.phenotype = phenotype;
    }

    public boolean hasActivePopulation() {
        return this.populationCount != 0;
    }

    public void update() {
        if (timeToNextUpdate == 0) {
            timeToNextUpdate = TIME_TO_UPDATE;
            populationCount += (growthQuanta * growthFactor) - (deathQuanta * deathFactor);

        } else {
            timeToNextUpdate--;
        }

        if (timeToNextWeatherUpdate == 0) {
            timeToNextWeatherUpdate = TIME_TO_WEATHER_UPDATE;
        } else {
            timeToNextWeatherUpdate--;
        }
    }


}
