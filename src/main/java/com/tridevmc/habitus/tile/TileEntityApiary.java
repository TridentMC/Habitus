/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.tile;

import com.elytradev.concrete.inventory.ConcreteItemStorage;
import com.elytradev.concrete.inventory.IContainerInventoryHolder;
import com.elytradev.concrete.inventory.ValidatedInventoryView;
import com.tridevmc.habitus.entity.EnumBeeBreed;
import com.tridevmc.habitus.entity.EnumBeeTemperament;
import com.tridevmc.habitus.generic.Color;
import com.tridevmc.habitus.gui.ApiaryInventoryView;
import com.tridevmc.habitus.gui.ILockableInventory;
import com.tridevmc.habitus.gui.container.ContainerApiary;
import com.tridevmc.habitus.item.ItemAssembledApiaryFrame;
import com.tridevmc.habitus.particle.ParticleBee;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import net.minecraft.block.BlockHorizontal;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3i;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.EnumSkyBlock;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.items.ItemStackHandler;

import java.util.List;

public class TileEntityApiary extends TileEntityBase implements ITEStackHandler,
    IContainerInventoryHolder, ITEBeePopulation {

    private final ConcreteItemStorage itemStackHandler;
    private String customName;
    public BeePopulation population;

    private static final int SLOT_COUNT = 3;

    public static final int FIELD_BEE_COUNT = 0;
    public static final int FIELD_BEE_TEMPERAMENT = 1;
    public static final int FIELD_BEE_BREED = 2;

    private boolean framesLocked = false;

    public TileEntityApiary() {
        itemStackHandler = new ApiaryItemStorage()
            .withValidators(this::isFrame, this::isFrame, this::isFrame)
            .withName(this.getName());
        population = new BeePopulation();
    }

    public BeePopulation getPopulation() {
        return population;
    }

    @Override
    public boolean hasPopulation() {
        return population.hasActivePopulation();
    }

    private boolean isFrame(@Nonnull ItemStack stack) {
        return (stack != ItemStack.EMPTY && stack.getItem() instanceof ItemAssembledApiaryFrame);
    }

    private boolean hasFrames() {
        return isFrame(itemStackHandler.getStackInSlot(0))
            || isFrame(itemStackHandler.getStackInSlot(1))
            || isFrame(itemStackHandler.getStackInSlot(2));
    }

    private int frameCount() {
        return (isFrame(itemStackHandler.getStackInSlot(0)) ? 1 : 0)
                + (isFrame(itemStackHandler.getStackInSlot(1)) ? 1 : 0)
                + (isFrame(itemStackHandler.getStackInSlot(2)) ? 1 : 0);
    }

    public EnumActionResult setPopulation(EntityPlayer user, BeePopulation population) {
        if (hasFrames()) {
            if(!this.world.isRemote) {
                this.population = population;
                this.world.notifyBlockUpdate(this.pos, this.world.getBlockState(this.pos), this.world.getBlockState(this.pos), 3);
                this.world.markBlockRangeForRenderUpdate(this.pos, this.pos);
                this.world.checkLightFor(EnumSkyBlock.BLOCK, this.pos);
                this.world.scheduleBlockUpdate(this.pos, this.getBlockType(), 0, 0);
            }
            return EnumActionResult.SUCCESS;
        } else {
            user.sendStatusMessage(new TextComponentTranslation("tile.habitus.apiary.noFrames"),
                true);
            return EnumActionResult.FAIL;
        }
    }

    @Override
    public BeePopulation getAndDestroyPopulation() {
        BeePopulation population = this.population.copy();
        this.population = new BeePopulation();
        return population;
    }

    @Nonnull
    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound update = new NBTTagCompound();
        this.writeToNBT(update);
        return update;
    }

    @Override
    @Nonnull
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound.setTag("inventory", itemStackHandler.serializeNBT());
        population.writeToNBT(compound);
        return super.writeToNBT(compound);
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        itemStackHandler.deserializeNBT(compound.getCompoundTag("inventory"));
        population.readFromNBT(compound);
    }

    @Override
    public int getField(int id) {
        switch (id) {
            case FIELD_BEE_COUNT:
                return population.getPopulationCount();
            case FIELD_BEE_TEMPERAMENT:
                return population.getTemperament().getId();
            case FIELD_BEE_BREED:
                return population.getBreed().getId();
            default:
                return 0;
        }
    }

    @Override
    public int getSlotCount() {
        return SLOT_COUNT;
    }


    @Override
    public void setField(int id, int value) {
        switch (id) {
            case FIELD_BEE_COUNT:
                population.setPopulationCount(value);
                break;
            case FIELD_BEE_TEMPERAMENT:
                population.setTemperament(EnumBeeTemperament.fromInt(value));
                break;
            case FIELD_BEE_BREED:
                population.setBreed(EnumBeeBreed.fromInt(value));
                break;
            default:
                break;
        }
    }

    @Override
    public void update() {

        if(!((ApiaryItemStorage)itemStackHandler).framesLocked() && this.population.hasActivePopulation() && frameCount() == 1) {
            ((ApiaryItemStorage)itemStackHandler).setFramesLocked(true);
        } else if(((ApiaryItemStorage)itemStackHandler).framesLocked() && (frameCount() > 1 || !this.population.hasActivePopulation())) {
            ((ApiaryItemStorage)itemStackHandler).setFramesLocked(false);
        }

        if (!this.world.isRemote && population.hasActivePopulation()) {
            population.update();

            if (world.rand.nextInt(20) <= 5) {
                List<EntityPlayer> closePlayers = world.getPlayers(EntityPlayer.class, (e) -> {
                    if (e != null) {
                        BlockPos currentPos = e.getPosition();
                        if (pos.getDistance(currentPos.getX(), currentPos.getY(), currentPos.getZ()) <= population.getTemperament().getAttackRange()) {
                            return e.attackable();
                        }
                    }
                    return false;
                });

                for (EntityPlayer player : closePlayers) {
                    player.attackEntityFrom(DamageSource.GENERIC, 0.5f);
                    population.getBreed().applyEffect(player);
                }
            }
        } else if(population.hasActivePopulation()) {
            Vec3i orientation = world.getBlockState(pos).getValue(BlockHorizontal.FACING)
                    .getDirectionVec();
            Vec3i offset;
            Color color = population.getTemperament().getColorForPlayer(Minecraft.getMinecraft().player);

            if (orientation.getZ() == 0) {
                offset = new Vec3i(orientation.getX() * (world.rand.nextDouble() * 5),
                        orientation.getY(), (world.rand.nextDouble() - 0.5) * 10);
            } else if (orientation.getX() == 0) {
                offset = new Vec3i((world.rand.nextDouble() - 0.5) * 10, orientation.getY(),
                        orientation.getZ() * (world.rand.nextDouble() * 5));
            } else {
                offset = new Vec3i(0, 0, 0);
            }

            ParticleBee bee = new ParticleBee(world, pos.getX() + orientation.getX() + 0.5,
                    pos.getY() + orientation.getY() + 0.5, pos.getZ() + orientation.getZ() + 0.5,
                    color.getRedFractional(), color.getGreenFractional(), color.getBlueFractional(), population.getBreed());
            bee.setTrackingPos(pos.add(offset));
            Minecraft.getMinecraft().effectRenderer.addEffect(bee);
        }
    }

    @Nullable
    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        return new SPacketUpdateTileEntity(this.pos, 1, this.getUpdateTag());
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        this.readFromNBT(pkt.getNbtCompound());
    }

    @Override
    @Nonnull
    public Container createContainer(@Nonnull InventoryPlayer playerInventory,
        @Nonnull EntityPlayer playerIn) {
        return new ContainerApiary(this, playerInventory, (ILockableInventory)this.getContainerInventory());
    }

    @Override
    @Nonnull
    public String getGuiID() {
        return "habitus:apiary";
    }

    @Override
    @Nonnull
    public String getName() {
        return this.hasCustomName() ? this.customName : "habitus.tileentity.apiary";
    }

    public void setCustomName(String name) {
        this.customName = name;
    }

    @Override
    public boolean hasCustomName() {
        return (this.customName != null && !this.customName.isEmpty());
    }

    @Override
    public boolean getActive() {
        return population.hasActivePopulation();
    }

    @Override
    public void setActive(boolean value) {

    }

    @Override
    public ItemStackHandler getItemStackHandler() {
        return this.itemStackHandler;
    }

    @Override
    public IInventory getContainerInventory() {
        ApiaryInventoryView result = new ApiaryInventoryView((ApiaryItemStorage)itemStackHandler);

        if (!this.world.isRemote) {
            return result
                .withField(FIELD_BEE_COUNT, () -> this.population.getPopulationCount())
                .withField(FIELD_BEE_TEMPERAMENT, () -> this.population.getTemperament().getId())
                .withField(FIELD_BEE_BREED, () -> this.population.getBreed().getId());
        }
        return result;
    }

    @Override
    public void writeItemData(NBTTagCompound compound) {

    }

    @Override
    public void readItemData(NBTTagCompound compound) {

    }
}
