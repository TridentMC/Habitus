/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.tile;

import com.elytradev.concrete.inventory.ConcreteItemStorage;
import com.elytradev.concrete.inventory.IContainerInventoryHolder;
import com.elytradev.concrete.inventory.ValidatedInventoryView;
import com.tridevmc.habitus.gui.container.ContainerBeeAnalyzer;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.util.EnumActionResult;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.items.ItemStackHandler;

public class TileEntityBeeAnalyzer extends TileEntityBase implements ITEStackHandler,
    IContainerInventoryHolder, ITEBeePopulation {

    private final ConcreteItemStorage itemStackHandler;
    private String customName;
    public BeePopulation population;

    private int analysisProgress = 0;
    private int analysisMax = 100;
    private boolean analyzing = false;
    private boolean analyzed = false;

    public static final int FIELD_ANALYSIS_PROGRESS = 0;
    public static final int FIELD_ANALYSIS_MAX = 1;
    public static final int FIELD_CONTAINER_STATE = 2;

    private static final int SLOT_COUNT = 2;

    public TileEntityBeeAnalyzer() {
        itemStackHandler = new ConcreteItemStorage(SLOT_COUNT)
            .withName(this.getName());
        population = new BeePopulation();
    }

    @Override
    public BeePopulation getPopulation() {
        return population;
    }

    @Override
    public boolean hasPopulation() {
        return population.hasActivePopulation();
    }

    public boolean isAnalyzing() {
        return analyzing;
    }

    public boolean wasAnalyzed() {
        return analyzed;
    }

    @Override
    public EnumActionResult setPopulation(EntityPlayer user, BeePopulation population) {
        this.population = population;
        analyzed = false;
        if(population != null && population.hasActivePopulation())
            analyzing = true;
        this.world.notifyBlockUpdate(pos, this.world.getBlockState(pos), this.world.getBlockState(pos), 3);
        this.markDirty();
        return EnumActionResult.SUCCESS;
    }

    @Override
    public BeePopulation getAndDestroyPopulation() {
        BeePopulation copy = population.copy();
        if(analyzing) {
            analyzing = false;
            analysisProgress = 0;
        }
        analyzed = false;
        population = new BeePopulation();
        this.world.notifyBlockUpdate(pos, this.world.getBlockState(pos), this.world.getBlockState(pos), 3);
        this.markDirty();
        return copy;
    }

    @Nonnull
    @Override
    public NBTTagCompound getUpdateTag() {
        NBTTagCompound update = new NBTTagCompound();
        this.writeToNBT(update);
        return update;
    }

    @Override
    @Nonnull
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound.setTag("inventory", itemStackHandler.serializeNBT());
        population.writeToNBT(compound);
        compound.setInteger("analysis_progress", analysisProgress);
        compound.setInteger("analysis_max", analysisMax);
        compound.setBoolean("has_been_analyzed", analyzed);
        return super.writeToNBT(compound);
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        itemStackHandler.deserializeNBT(compound.getCompoundTag("inventory"));
        analysisProgress = compound.getInteger("analysis_progress");
        analysisMax = compound.getInteger("analysis_max");
        analyzed = compound.getBoolean("has_been_analyzed");
        population.readFromNBT(compound);
    }

    @Override
    public int getField(int id) {
        switch(id) {
            case FIELD_ANALYSIS_PROGRESS:
                return analysisProgress;
            case FIELD_ANALYSIS_MAX:
                return analysisMax;
            default:
                return 0;
        }
    }

    @Override
    public int getSlotCount() {
        return SLOT_COUNT;
    }


    @Override
    public void setField(int id, int value) {
        switch(id) {
            case FIELD_ANALYSIS_PROGRESS:
                analysisProgress = value;
            case FIELD_ANALYSIS_MAX:
                analysisMax = value;
            default:
                break;
        }
    }

    @Override
    public void update() {
		if(!this.world.isRemote) {
		    if(this.analyzing) {
                if (++analysisProgress >= analysisMax) {
                    analysisProgress = 0;
                    analyzing = false;
                    analyzed = true;
                    this.markDirty();
                    this.world.notifyBlockUpdate(pos, this.world.getBlockState(pos), this.world.getBlockState(pos), 3);
                }
            } else if(analysisProgress != 0) {
		        analyzing = true;
            }
		}
    }

    @Nullable
    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        return new SPacketUpdateTileEntity(this.pos, 1, this.getUpdateTag());
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        this.readFromNBT(pkt.getNbtCompound());
    }

    @Override
    @Nonnull
    public Container createContainer(@Nonnull InventoryPlayer playerInventory,
        @Nonnull EntityPlayer playerIn) {
        return new ContainerBeeAnalyzer(this, playerInventory, this.getContainerInventory());
    }

    @Override
    @Nonnull
    public String getGuiID() {
        return "habitus:bee_analyzer";
    }

    @Override
    @Nonnull
    public String getName() {
        return this.hasCustomName() ? this.customName : "habitus.tileentity.bee_analyzer";
    }

    public void setCustomName(String name) {
        this.customName = name;
    }

    @Override
    public boolean hasCustomName() {
        return (this.customName != null && !this.customName.isEmpty());
    }

    @Override
    public boolean getActive() {
        return population.hasActivePopulation();
    }

    @Override
    public void setActive(boolean value) {

    }

    @Override
    public ItemStackHandler getItemStackHandler() {
        return this.itemStackHandler;
    }

    @Override
    public IInventory getContainerInventory() {
        ValidatedInventoryView result = new ValidatedInventoryView(itemStackHandler);

        if (!this.world.isRemote) {
            return result
                    .withField(FIELD_ANALYSIS_PROGRESS, () -> this.analysisProgress)
                    .withField(FIELD_ANALYSIS_MAX, () -> this.analysisMax);
        }

        return result;
    }

    @Override
    public void writeItemData(NBTTagCompound compound) {

    }

    @Override
    public void readItemData(NBTTagCompound compound) {

    }
}
