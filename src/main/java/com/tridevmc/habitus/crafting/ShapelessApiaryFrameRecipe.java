/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.crafting;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.tridevmc.habitus.init.HSItems;
import com.tridevmc.habitus.item.ItemApiaryFoundation;
import com.tridevmc.habitus.item.ItemApiaryFrame;
import com.tridevmc.habitus.item.ItemBalancedApiaryFrame;
import javax.annotation.Nonnull;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.common.crafting.CraftingHelper;
import net.minecraftforge.common.crafting.IRecipeFactory;
import net.minecraftforge.common.crafting.JsonContext;
import net.minecraftforge.oredict.ShapelessOreRecipe;


class ShapelessApiaryFrameRecipe extends ShapelessOreRecipe {

    private ShapelessApiaryFrameRecipe(ResourceLocation group, NonNullList<Ingredient> input,
        @Nonnull ItemStack result) {
        super(group, input, result);
    }

    @Nonnull
    @Override
    public ItemStack getCraftingResult(@Nonnull InventoryCrafting inv) {
        ItemStack frame = ItemStack.EMPTY;
        ItemStack foundation = ItemStack.EMPTY;
        for (int i = 0; i < inv.getSizeInventory(); i++) {
            ItemStack ing = inv.getStackInSlot(i);

            if (ing == ItemStack.EMPTY) {
                continue;
            }

            if (ing.getItem() instanceof ItemApiaryFrame) {
                if (frame != ItemStack.EMPTY) {
                    return ItemStack.EMPTY;
                }

                frame = ing.copy();
            }

            if (ing.getItem() instanceof ItemApiaryFoundation) {
                if (foundation != ItemStack.EMPTY) {
                    return ItemStack.EMPTY;
                }

                foundation = ing.copy();
            }
        }

        if (frame == ItemStack.EMPTY || foundation == ItemStack.EMPTY) {
            return ItemStack.EMPTY;
        }

        NBTTagCompound compound = new NBTTagCompound();
        compound.setTag("frame", ((ItemApiaryFrame) frame.getItem()).serializeToNBT(frame));
        compound.setTag("foundation",
            ((ItemApiaryFoundation) foundation.getItem()).serializeToNBT(foundation));

        ItemStack product = ItemStack.EMPTY;
        if (frame.getItem() instanceof ItemBalancedApiaryFrame) {
            product = new ItemStack(HSItems.assembledBalancedApiaryFrame, 1);
        } else {
            product = new ItemStack(HSItems.assembledApiaryFrame, 1);
        }
        product.setTagCompound(compound);
        return product;
    }

    @Override
    public boolean isDynamic() {
        return true;
    }

    @Override
    public boolean matches(@Nonnull InventoryCrafting inv, @Nonnull World world) {
        boolean foundFrame = false, foundFoundation = false;
        for (int i = 0; i < inv.getSizeInventory(); i++) {
            ItemStack ing = inv.getStackInSlot(i);
            if (ing == ItemStack.EMPTY) {
                continue;
            }

            if (ing.getItem() instanceof ItemApiaryFrame) {
                if (foundFrame) {
                    return false;
                }
                foundFrame = true;
            }

            if (ing.getItem() instanceof ItemApiaryFoundation) {
                if (foundFoundation) {
                    return false;
                }
                foundFoundation = true;
            }
        }

        return foundFrame && foundFoundation;
    }

    @SuppressWarnings({"WeakerAccess", "unused"})
    public static class Factory implements IRecipeFactory {

        @Override
        public IRecipe parse(final JsonContext context, final JsonObject json) {
            final String group = JsonUtils.getString(json, "group", "");
            final NonNullList<Ingredient> ingredients = NonNullList.create();
            for (final JsonElement element : JsonUtils.getJsonArray(json, "ingredients")) {
                ingredients.add(CraftingHelper.getIngredient(element, context));
            }

            if (ingredients.isEmpty()) {
                throw new JsonParseException("No ingredients for shapeless recipe");
            }

            final ItemStack result = CraftingHelper
                .getItemStack(JsonUtils.getJsonObject(json, "result"), context);

            return new ShapelessApiaryFrameRecipe(
                group.isEmpty() ? null : new ResourceLocation(group), ingredients, result);
        }
    }
}
