/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.crafting;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.tridevmc.habitus.init.HSItems;
import javax.annotation.Nonnull;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemPotion;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.common.crafting.CraftingHelper;
import net.minecraftforge.common.crafting.IRecipeFactory;
import net.minecraftforge.common.crafting.JsonContext;
import net.minecraftforge.oredict.ShapelessOreRecipe;


class ShapelessFireproofingOilRecipe extends ShapelessOreRecipe {

    private ShapelessFireproofingOilRecipe(ResourceLocation group, NonNullList<Ingredient> input,
        @Nonnull ItemStack result) {
        super(group, input, result);
    }

    @Nonnull
    @Override
    public ItemStack getCraftingResult(@Nonnull InventoryCrafting inv) {
        int waterCount = 0;
        boolean hasDust = false;
        for (int i = 0; i < inv.getSizeInventory(); i++) {
            ItemStack ing = inv.getStackInSlot(i);

            if (ing == ItemStack.EMPTY) {
                continue;
            }

            if (ing.getItem() instanceof ItemPotion) {
                if (waterCount >= 3) {
                    return ItemStack.EMPTY;
                }

                if (ing.getTagCompound() != null && ing.getTagCompound().getString("Potion")
                    .equalsIgnoreCase("minecraft:water")) {
                    waterCount++;
                } else {
                    return ItemStack.EMPTY;
                }
            }

            if (ing.getItem().getUnlocalizedName().equals("item.habitus.helleton_dust")) {
                if (hasDust) {
                    return ItemStack.EMPTY;
                }

                hasDust = true;
            }
        }

        if (waterCount != 3 || !hasDust) {
            return ItemStack.EMPTY;
        }

        return new ItemStack(HSItems.fireproofingOil, 3);
    }

    @Override
    public boolean matches(@Nonnull InventoryCrafting inv, @Nonnull World world) {
        return getCraftingResult(inv) != ItemStack.EMPTY;
    }

    @SuppressWarnings({"WeakerAccess", "unused"})
    public static class Factory implements IRecipeFactory {

        @Override
        public IRecipe parse(final JsonContext context, final JsonObject json) {
            final String group = JsonUtils.getString(json, "group", "");
            final NonNullList<Ingredient> ingredients = NonNullList.create();
            for (final JsonElement element : JsonUtils.getJsonArray(json, "ingredients")) {
                ingredients.add(CraftingHelper.getIngredient(element, context));
            }

            if (ingredients.isEmpty()) {
                throw new JsonParseException("No ingredients for shapeless recipe");
            }

            final ItemStack result = CraftingHelper
                .getItemStack(JsonUtils.getJsonObject(json, "result"), context);

            return new ShapelessFireproofingOilRecipe(
                group.isEmpty() ? null : new ResourceLocation(group), ingredients, result);
        }
    }
}
