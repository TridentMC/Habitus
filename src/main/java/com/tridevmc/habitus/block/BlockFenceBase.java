package com.tridevmc.habitus.block;

import com.tridevmc.habitus.Habitus;
import net.minecraft.block.BlockFence;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemBlock;

import javax.annotation.Nonnull;
import java.util.Objects;

public class BlockFenceBase extends BlockFence implements IHabitusBlock {

    protected final String name;

    public BlockFenceBase(Material materialIn, MapColor mapColorIn, String name) {
        super(materialIn, mapColorIn);
        this.name = name + "_fence";
        this.setRegistryName(name + "_fence");
        this.setUnlocalizedName("habitus." + name + "_fence");
        this.setCreativeTab(Habitus.habitusTab);
    }

    @Override
    public void registerItemModel(ItemBlock itemBlock) {
        Habitus.proxy.registerItemRenderer(itemBlock, 0, name);
    }

    @Override
    public ItemBlock getItemBlock() {
        return (ItemBlock) new ItemBlock(this)
                .setRegistryName(Objects.requireNonNull(this.getRegistryName()));
    }

    @Override
    @Nonnull
    public BlockFenceBase setCreativeTab(@Nonnull CreativeTabs tab) {
        super.setCreativeTab(tab);
        return this;
    }
}
