/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.block;

import com.tridevmc.habitus.Habitus;
import com.tridevmc.habitus.init.HSBlocks;
import com.tridevmc.habitus.item.ItemBlockFPPlanksVanilla;
import net.minecraft.block.BlockPlanks;
import net.minecraft.block.SoundType;
import net.minecraft.client.resources.I18n;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemMultiTexture;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;

public class BlockFPPlanksVanilla extends BlockPlanks implements IHabitusBlock {

    public BlockFPPlanksVanilla() {
        this.setRegistryName("fp_planks_vanilla");
        this.setCreativeTab(Habitus.habitusTab);
        this.setHardness(2.0F);
        this.setResistance(5.0F);
        this.setSoundType(SoundType.WOOD);
    }

    @Override
    public int getFireSpreadSpeed(IBlockAccess world, BlockPos pos, EnumFacing face) {
        return 0;
    }

    @Override
    public int getFlammability(IBlockAccess world, BlockPos pos, EnumFacing face) {
        return 0;
    }

    @Override
    public String getUnlocalizedName() {
        return "tile.habitus";
    }

    @Override
    public void getSubBlocks(CreativeTabs tab, NonNullList<ItemStack> items) {
        if (tab != Habitus.habitusTab) {
            return;
        }
        for (BlockPlanks.EnumType type : BlockPlanks.EnumType.values()) {
            items.add(new ItemStack(this, 1, type.getMetadata()));
        }
    }

    @Override
    public void registerItemModel(ItemBlock itemBlock) {
        for (BlockPlanks.EnumType type : BlockPlanks.EnumType.values()) {
            Habitus.proxy.registerItemRenderer(itemBlock, type.getMetadata(), "fp_planks_vanilla",
                "variant=" + type.getName());
        }
    }

    @Override
    public ItemBlock getItemBlock() {
        return (ItemBlock) (new ItemBlockFPPlanksVanilla()).setRegistryName("fp_planks_vanilla");

    }
}
