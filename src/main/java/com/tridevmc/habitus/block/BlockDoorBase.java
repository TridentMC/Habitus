/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.block;

import net.minecraft.block.BlockDoor;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemBlock;

public class BlockDoorBase extends BlockDoor implements IHabitusBlock {

    protected final String name;

    public BlockDoorBase(Material materialIn, String name) {
        super(materialIn);
        this.name = name;
        this.setRegistryName(name + "_door");
        this.setUnlocalizedName("habitus." + name + "_door");
    }

    @Override
    public void registerItemModel(ItemBlock itemBlock) {

    }

    @Override
    public ItemBlock getItemBlock() {
        return null;
    }

    public BlockDoorBase setSoundType(SoundType type) {
        super.setSoundType(type);
        return this;
    }

    @Override
    public BlockDoorBase setHardness(float hardness) {
        super.setHardness(hardness);
        return this;
    }
}
