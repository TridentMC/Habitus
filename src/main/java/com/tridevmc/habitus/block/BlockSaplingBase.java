/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.block;

import com.tridevmc.habitus.Habitus;
import com.tridevmc.habitus.generic.IOreDict;
import java.util.Objects;
import java.util.Random;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import net.minecraft.block.BlockBush;
import net.minecraft.block.IGrowable;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.oredict.OreDictionary;

@SuppressWarnings("deprecation")
public abstract class BlockSaplingBase extends BlockBush implements IHabitusBlock, IGrowable,
    IOreDict {

    private final String name;
    private static final PropertyInteger STAGE = PropertyInteger.create("stage", 0, 1);
    private static final AxisAlignedBB SAPLING_AABB = new AxisAlignedBB(0.09999999403953552D, 0.0D,
        0.09999999403953552D, 0.8999999761581421D, 0.800000011920929D, 0.8999999761581421D);

    BlockSaplingBase(String name) {
        this.name = name;
        this.setUnlocalizedName("habitus." + name + "_sapling");
        this.setRegistryName(name + "_sapling");
        this.setDefaultState(this.blockState.getBaseState().withProperty(STAGE, 0));
        this.setCreativeTab(Habitus.habitusTab);
    }

    @Override
    @Nonnull
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos) {
        return SAPLING_AABB;
    }

    @Override
    public void updateTick(@Nullable World worldIn, @Nullable BlockPos pos,
        @Nullable IBlockState state, Random rand) {
        if (worldIn != null && !worldIn.isRemote) {
            super.updateTick(worldIn, pos, state, rand);

            if (pos != null && state != null && worldIn.getLightFromNeighbors(pos.up()) >= 9
                && rand.nextInt(7) == 0) {
                this.grow(worldIn, rand, pos, state);
            }
        }
    }

    @Override
    @Nonnull
    public String getLocalizedName() {
        return I18n.translateToLocal(this.getUnlocalizedName());
    }

    abstract void generateTree(World worldIn, BlockPos pos, IBlockState state, Random rand);


    @Override
    public int damageDropped(IBlockState state) {
        return 0;
    }

    @Override
    @Nonnull
    public IBlockState getStateFromMeta(int meta) {
        return this.getDefaultState().withProperty(STAGE, (meta & 8) >> 3);
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        int i = 0;
        i = i | state.getValue(STAGE) << 3;
        return i;
    }

    @Override
    @Nonnull
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, STAGE);
    }

    @Override
    public ItemBlock getItemBlock() {
        return (ItemBlock) new ItemBlock(this)
            .setRegistryName(Objects.requireNonNull(this.getRegistryName()));
    }

    @Override
    public void registerItemModel(ItemBlock itemBlock) {
        Habitus.proxy.registerItemRenderer(itemBlock, 0, name + "_sapling");
    }

    @Override
    public boolean canGrow(@Nonnull World worldIn, @Nonnull BlockPos pos,
        @Nonnull IBlockState state, boolean isClient) {
        return true;
    }

    @Override
    public boolean canUseBonemeal(@Nonnull World worldIn, @Nonnull Random rand,
        @Nonnull BlockPos pos, @Nonnull IBlockState state) {
        return true;
    }

    @Override
    public void grow(@Nonnull World worldIn, @Nonnull Random rand, @Nonnull BlockPos pos,
        @Nonnull IBlockState state) {
        if (state.getValue(STAGE) == 0) {
            worldIn.setBlockState(pos, state.cycleProperty(STAGE), 4);
        } else {
            this.generateTree(worldIn, pos, state, rand);
        }
    }

    @Override
    public void registerOreDict() {
        OreDictionary
            .registerOre("treeSapling", new ItemStack(this, 1, OreDictionary.WILDCARD_VALUE));
    }

    @Override
    @Nonnull
    public BlockSaplingBase setCreativeTab(@Nonnull CreativeTabs tab) {
        super.setCreativeTab(tab);
        return this;
    }

    @Override
    public CreativeTabs getCreativeTabToDisplayOn() {
        return Habitus.habitusTab;
    }
}
