/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.block;

import com.tridevmc.habitus.Habitus;
import java.util.Random;
import javax.annotation.Nonnull;
import net.minecraft.block.BlockSlab;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

@SuppressWarnings("deprecation")
abstract class BlockSlabBase extends BlockSlab implements IHabitusBlock {

    private static final PropertyEnum<BlockSlabBase.Variant> VARIANT = PropertyEnum
        .create("variant", BlockSlabBase.Variant.class);

    private final String name;

    BlockSlabBase(Material materialIn, String name) {
        super(materialIn);
        this.name = name;
        this.setUnlocalizedName("habitus." + name + (this.isDouble() ? "_double" : "") + "_slab");
        this.setRegistryName(name + (this.isDouble() ? "_double" : "") + "_slab");
        this.useNeighborBrightness = true;
        IBlockState iblockstate = this.blockState.getBaseState();

        if (!this.isDouble()) {
            iblockstate = iblockstate.withProperty(HALF, BlockSlab.EnumBlockHalf.BOTTOM);
        }

        this.setDefaultState(iblockstate.withProperty(VARIANT, BlockSlabBase.Variant.DEFAULT));
        this.setCreativeTab(Habitus.habitusTab);
    }

    protected abstract Item getDropItem();

    protected abstract ItemStack getDropStack();

    @Override
    @Nonnull
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return getDropItem();
    }

    @Override
    @Nonnull
    public ItemStack getItem(World worldIn, BlockPos pos, @Nonnull IBlockState state) {
        return getDropStack();
    }

    @Override
    @Nonnull
    public IBlockState getStateFromMeta(int meta) {
        IBlockState iblockstate = this.getDefaultState()
            .withProperty(VARIANT, BlockSlabBase.Variant.DEFAULT);

        if (!this.isDouble()) {
            iblockstate = iblockstate.withProperty(HALF,
                (meta & 8) == 0 ? BlockSlab.EnumBlockHalf.BOTTOM : BlockSlab.EnumBlockHalf.TOP);
        }

        return iblockstate;
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        int i = 0;

        if (!this.isDouble() && state.getValue(HALF) == BlockSlab.EnumBlockHalf.TOP) {
            i |= 8;
        }

        return i;
    }

    @Override
    @Nonnull
    protected BlockStateContainer createBlockState() {
        return this.isDouble() ? new BlockStateContainer(this, VARIANT)
            : new BlockStateContainer(this, HALF, VARIANT);
    }

    @Override
    @Nonnull
    public String getUnlocalizedName(int meta) {
        return super.getUnlocalizedName();
    }

    @Override
    @Nonnull
    public IProperty<?> getVariantProperty() {
        return VARIANT;
    }

    @Override
    @Nonnull
    public Comparable<?> getTypeForItem(@Nonnull ItemStack stack) {
        return BlockSlabBase.Variant.DEFAULT;
    }


    public enum Variant implements IStringSerializable {
        DEFAULT;

        @Override
        public String getName() {
            return "default";
        }
    }

    @Override
    public void registerItemModel(ItemBlock itemBlock) {
        Habitus.proxy.registerItemRenderer(itemBlock, 0, name + "_slab");
    }

}
