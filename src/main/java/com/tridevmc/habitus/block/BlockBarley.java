/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.block;

import com.tridevmc.habitus.Habitus;
import com.tridevmc.habitus.init.HSItems;
import java.util.Objects;
import javax.annotation.Nonnull;
import net.minecraft.block.BlockCrops;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;

public class BlockBarley extends BlockCrops implements IHabitusBlock {

    public BlockBarley() {
        super();
        this.setRegistryName("barley");
        this.setUnlocalizedName("habitus.barley");
    }

    @Override
    @Nonnull
    protected Item getSeed() {
        return HSItems.barleySeeds;
    }

    @Override
    @Nonnull
    protected Item getCrop() {
        return HSItems.barley;
    }

    @Override
    public ItemBlock getItemBlock() {
        return (ItemBlock) new ItemBlock(this)
            .setRegistryName(Objects.requireNonNull(this.getRegistryName()));
    }

    @Override
    public void registerItemModel(ItemBlock itemBlock) {
        Habitus.proxy.registerItemRenderer(itemBlock, 0, "barley");
    }
}
