/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.block;

import com.tridevmc.habitus.entity.EntityBeeSwarm;
import com.tridevmc.habitus.init.GuiHandler;
import com.tridevmc.habitus.tile.TileEntityApiary;
import java.util.Objects;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockApiary extends BlockTEBase<TileEntityApiary> {

    public BlockApiary() {
        super(Material.WOOD, "apiary", GuiHandler.GUI_APIARY);
        this.setSoundType(SoundType.WOOD);
    }

    @Override
    public Class<TileEntityApiary> getTileEntityClass() {
        return TileEntityApiary.class;
    }

    @Nullable
    @Override
    public TileEntityApiary createTileEntity(@Nonnull World world, @Nonnull IBlockState state) {
        return new TileEntityApiary();
    }

    @Override
    public void breakBlock(@Nonnull World worldIn, @Nonnull BlockPos pos,
        @Nonnull IBlockState state) {
        TileEntity te = worldIn.getTileEntity(pos);
        if (te instanceof TileEntityApiary) {
            TileEntityApiary apiary = (TileEntityApiary) worldIn.getTileEntity(pos);
            if (Objects.requireNonNull(apiary).population.getPopulationCount() != 0) {
                EntityBeeSwarm beeSwarm = EntityBeeSwarm
                    .beeSwarmOf(worldIn, pos, apiary.population);
                beeSwarm.setBeePopulation(apiary.population);
                worldIn.spawnEntity(beeSwarm);
            }
        }

        super.breakBlock(worldIn, pos, state);
    }

}
