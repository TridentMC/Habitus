/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.block;

import com.tridevmc.habitus.init.HSFluids;
import javax.annotation.Nonnull;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class BlockFluidHoney extends BlockFluidBaseClassic {

    public BlockFluidHoney() {
        super(HSFluids.fluid_honey, Material.WATER, "honey");
    }

    @Override
    public Vec3d getFogColor(World world, BlockPos pos, IBlockState state, Entity entity,
        Vec3d originalColor, float partialTicks) {
        return new Vec3d(1.00, 0.80, 0.02);
    }

    @Nonnull
    @Override
    public Vec3d modifyAcceleration(@Nonnull World world, @Nonnull BlockPos pos,
        @Nonnull Entity entity, @Nonnull Vec3d vec) {
        return super.modifyAcceleration(world, pos, entity, vec).scale(0.25);
    }


}
