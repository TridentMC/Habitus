package com.tridevmc.habitus.block;

import com.tridevmc.habitus.Habitus;
import net.minecraft.block.BlockFenceGate;
import net.minecraft.block.BlockPlanks;
import net.minecraft.block.material.MapColor;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemBlock;

import javax.annotation.Nonnull;
import java.util.Objects;

public class BlockFenceGateBase extends BlockFenceGate implements IHabitusBlock {

    protected final String name;

    public BlockFenceGateBase(MapColor color, String name) {
        super(BlockPlanks.EnumType.OAK);
        this.name = name + "_fence_gate";
        this.setRegistryName(name + "_fence_gate");
        this.setUnlocalizedName("habitus." + name + "_fence_gate");
        this.setCreativeTab(Habitus.habitusTab);
    }

    @Override
    public void registerItemModel(ItemBlock itemBlock) {
        Habitus.proxy.registerItemRenderer(itemBlock, 0, name);
    }

    @Override
    public ItemBlock getItemBlock() {
        return (ItemBlock) new ItemBlock(this)
                .setRegistryName(Objects.requireNonNull(this.getRegistryName()));
    }

    @Override
    @Nonnull
    public BlockFenceGateBase setCreativeTab(@Nonnull CreativeTabs tab) {
        super.setCreativeTab(tab);
        return this;
    }
}
