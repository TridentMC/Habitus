/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.block;

import com.tridevmc.habitus.Habitus;
import net.minecraft.block.BlockHay;
import net.minecraft.block.SoundType;
import net.minecraft.item.ItemBlock;

import java.util.Objects;

public class BlockBarleyBale extends BlockHay implements IHabitusBlock {
    public BlockBarleyBale() {
        super();
        this.setRegistryName("barley_bale");
        this.setUnlocalizedName("habitus.barley_bale");
        this.setCreativeTab(Habitus.habitusTab);
        this.setSoundType(SoundType.PLANT);
        this.setHardness(0.5f);
    }

    @Override
    public void registerItemModel(ItemBlock itemBlock) {
        Habitus.proxy.registerItemRenderer(itemBlock, 0, "barley_bale");
    }

    @Override
    public ItemBlock getItemBlock() {
        return (ItemBlock) new ItemBlock(this)
                .setRegistryName(Objects.requireNonNull(this.getRegistryName()));
    }
}
