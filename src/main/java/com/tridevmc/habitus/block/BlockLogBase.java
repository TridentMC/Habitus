/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.block;

import static net.minecraft.block.BlockLog.EnumAxis.X;

import com.tridevmc.habitus.generic.IOreDict;
import javax.annotation.Nonnull;
import net.minecraft.block.BlockLog;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.oredict.OreDictionary;

@SuppressWarnings("deprecation")
public class BlockLogBase extends BlockBase implements IOreDict {

    private static final PropertyEnum<BlockLog.EnumAxis> AXIS = PropertyEnum
        .create("axis", BlockLog.EnumAxis.class);

    public BlockLogBase(String name) {
        super(Material.WOOD, name + "_log");
        this.setHardness(2.0F);
        this.setDefaultState(
            this.blockState.getBaseState().withProperty(AXIS, BlockLog.EnumAxis.Y));
        this.setSoundType(SoundType.WOOD);
    }

    @Override
    @Nonnull
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, AXIS);
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        switch (state.getValue(AXIS)) {
            case X:
                return 0b0000;
            case Y:
                return 0b0100;
            case Z:
                return 0b1100;
            default:
                return 0b0000;
        }
    }

    @Override
    @Nonnull
    public IBlockState getStateFromMeta(int meta) {
        switch (meta & 0b1100) {
            case 0b0000:
                return this.getDefaultState().withProperty(AXIS, BlockLog.EnumAxis.X);
            case 0b0100:
                return this.getDefaultState().withProperty(AXIS, BlockLog.EnumAxis.Y);
            case 0b1100:
                return this.getDefaultState().withProperty(AXIS, BlockLog.EnumAxis.Z);
            default:
                return this.getDefaultState();
        }
    }

    @Override
    @Nonnull
    public IBlockState getStateForPlacement(World worldIn, BlockPos pos, EnumFacing facing,
        float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer) {
        return this.getStateFromMeta(meta)
            .withProperty(AXIS, BlockLog.EnumAxis.fromFacingAxis(facing.getAxis()));
    }

    @Override
    @Nonnull
    public IBlockState withRotation(@Nonnull IBlockState state, Rotation rot) {
        switch (rot) {
            case COUNTERCLOCKWISE_90:
            case CLOCKWISE_90:

                switch (state.getValue(AXIS)) {
                    case X:
                        return state.withProperty(AXIS, BlockLog.EnumAxis.Z);
                    case Z:
                        return state.withProperty(AXIS, X);
                    default:
                        return state;
                }

            default:
                return state;
        }
    }

    @Override
    public boolean isWood(IBlockAccess world, BlockPos pos) {
        return true;
    }

    @Override
    public boolean canSustainLeaves(IBlockState state, IBlockAccess world, BlockPos pos) {
        return true;
    }

    @Override
    @Nonnull
    protected ItemStack getSilkTouchDrop(@Nonnull IBlockState state) {
        return new ItemStack(getItemBlock(), 1, 0);
    }

    @Override
    public void registerOreDict() {
        OreDictionary.registerOre("logWood", new ItemStack(this, 1, OreDictionary.WILDCARD_VALUE));
    }
}
