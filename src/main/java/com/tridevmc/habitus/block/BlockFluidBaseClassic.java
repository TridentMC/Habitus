/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.block;

import com.tridevmc.habitus.Habitus;
import java.util.Objects;
import javax.annotation.Nonnull;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.block.statemap.StateMapperBase;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fluids.BlockFluidClassic;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;


public abstract class BlockFluidBaseClassic extends BlockFluidClassic implements IHabitusBlock {

    private final String name;

    public BlockFluidBaseClassic(Fluid fluid, Material material, String name) {
        super(fluid, material);
        this.name = name;
        setUnlocalizedName(Habitus.MOD_ID + "." + name);
        setRegistryName(name);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerItemModel(ItemBlock itemBlock) {
        ModelResourceLocation fluidLocation = new ModelResourceLocation(Habitus.MOD_ID + ":fluid",
            name);
        ModelLoader.registerItemVariants(itemBlock);
        ModelLoader.setCustomMeshDefinition(itemBlock, stack -> fluidLocation);
        ModelLoader.setCustomStateMapper(itemBlock.getBlock(), new StateMapperBase() {
            @Override
            @Nonnull
            protected ModelResourceLocation getModelResourceLocation(@Nonnull IBlockState state) {
                return fluidLocation;
            }
        });
    }

    @Override
    public ItemBlock getItemBlock() {
        return (ItemBlock) new ItemBlock(this)
            .setRegistryName(Objects.requireNonNull(this.getRegistryName()));
    }

}
