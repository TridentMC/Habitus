/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.block;

import com.tridevmc.habitus.generic.IOreDict;
import com.tridevmc.habitus.init.HSBlocks;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemSlab;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;

import java.util.Objects;

public abstract class BlockSlabWhiteCherry extends BlockSlabBase {

    private BlockSlabWhiteCherry() {
        super(Material.WOOD, "cherry_white");
        this.setHardness(2.0F);
        this.setResistance(5.0F);
    }

    @Override
    public Item getDropItem() {
        return Item.getItemFromBlock(HSBlocks.whiteCherrySlab);
    }

    @Override
    public ItemStack getDropStack() {
        return new ItemStack(HSBlocks.whiteCherrySlab);
    }

    @Override
    public ItemBlock getItemBlock() {
        return (ItemBlock) new ItemSlab(HSBlocks.whiteCherrySlab, HSBlocks.whiteCherrySlab,
            HSBlocks.doubleWhiteCherrySlab)
            .setRegistryName(Objects.requireNonNull(this.getRegistryName()));
    }

    public static class Double extends BlockSlabWhiteCherry {

        @Override
        public boolean isDouble() {
            return true;
        }
    }

    public static class Half extends BlockSlabWhiteCherry implements IOreDict {

        @Override
        public boolean isDouble() {
            return false;
        }

        @Override
        public void registerOreDict() {
            OreDictionary
                .registerOre("woodSlab", new ItemStack(this, 1, OreDictionary.WILDCARD_VALUE));
        }
    }
}
