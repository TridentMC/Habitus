/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.block;

import com.tridevmc.habitus.Habitus;
import java.util.Objects;
import net.minecraft.block.BlockStairs;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.ItemBlock;

abstract class BlockStairsBase extends BlockStairs implements IHabitusBlock {

    private final String name;

    BlockStairsBase(IBlockState modelState, String name) {
        super(modelState);
        this.name = name;
        this.setRegistryName(name + "_stairs");
        this.setUnlocalizedName("habitus." + name + "_stairs");
        this.setCreativeTab(Habitus.habitusTab);
        this.useNeighborBrightness = true;
    }

    @Override
    public void registerItemModel(ItemBlock itemBlock) {
        Habitus.proxy.registerItemRenderer(itemBlock, 0, name + "_stairs");
    }

    @Override
    public ItemBlock getItemBlock() {
        return (ItemBlock) new ItemBlock(this)
            .setRegistryName(Objects.requireNonNull(this.getRegistryName()));
    }
}
