/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.block;

import com.tridevmc.habitus.Habitus;
import java.util.Objects;
import javax.annotation.Nonnull;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemBlock;

public class BlockBase extends Block implements IHabitusBlock {

    private final String name;

    public BlockBase(Material materialIn, String name) {
        super(materialIn);

        this.name = name;

        setUnlocalizedName("habitus." + name);
        setRegistryName(name);
        this.setCreativeTab(Habitus.habitusTab);
    }

    @Override
    public ItemBlock getItemBlock() {
        return (ItemBlock) new ItemBlock(this)
            .setRegistryName(Objects.requireNonNull(this.getRegistryName()));
    }

    @Override
    public void registerItemModel(ItemBlock itemBlock) {
        Habitus.proxy.registerItemRenderer(itemBlock, 0, name);
    }

    @Override
    @Nonnull
    public BlockBase setCreativeTab(@Nonnull CreativeTabs tab) {
        super.setCreativeTab(tab);
        return this;
    }

    @Override
    @Nonnull
    public BlockBase setHardness(float hardness) {
        super.setHardness(hardness);
        return this;
    }

    @Override
    @Nonnull
    public BlockBase setResistance(float resistance) {
        super.setResistance(resistance);
        return this;
    }

    @Override
    @Nonnull
    public BlockBase setSoundType(@Nonnull SoundType sound) {
        super.setSoundType(sound);
        return this;
    }
}
