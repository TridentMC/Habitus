/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.entity;

import com.tridevmc.habitus.Habitus;
import com.tridevmc.habitus.generic.Color;
import com.tridevmc.habitus.init.HSSounds;
import com.tridevmc.habitus.particle.ParticleBee;
import com.tridevmc.habitus.tile.BeePopulation;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAttackMelee;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.World;

public abstract class EntityBeeSwarm extends EntityBaseSwarm {

    private static final DataParameter<NBTTagCompound> BEE_POPULATION = EntityDataManager
        .createKey(EntityBeeSwarm.class, DataSerializers.COMPOUND_TAG);

    private EnumBeeBreed breed;
    private boolean isPartying = false;

    EntityBeeSwarm(World worldIn) {
        super(worldIn);
        this.setBeePopulation(new BeePopulation());
    }

    public static EntityBeeSwarm beeSwarmOf(World worldIn, BlockPos pos, BeePopulation population) {
        EntityBeeSwarm swarm;
        switch (population.getBreed()) {
            case TEMPERATE:
                swarm = new EntityTemperateBeeSwarm(worldIn);
                break;
            case FRIGID:
                swarm = new EntityFrigidBeeSwarm(worldIn);
                break;
            case ARID:
                swarm = new EntityAridBeeSwarm(worldIn);
                break;
            case SWAMPY:
                swarm = new EntitySwampyBeeSwarm(worldIn);
                break;
            case HELLISH:
                swarm = new EntityHellishBeeSwarm(worldIn);
                break;
            case FUNGAL:
                swarm = new EntityFungalBeeSwarm(worldIn);
                break;
            case CERAMIC:
                swarm = new EntityCeramicBeeSwarm(worldIn);
                break;
            case TRANSCENDENT:
                swarm = new EntityTranscendentBeeSwarm(worldIn);
                break;
            case HUMID:
                swarm = new EntityHumidBeeSwarm(worldIn);
                break;
            case WITHERED:
                swarm = new EntityWitheredBeeSwarm(worldIn);
                break;
            default:
                swarm = new EntityTemperateBeeSwarm(worldIn);
                Habitus.LOG.warn("Found weird breed '" + population.getBreed().getName()
                    + "' - changing to Temperate");
                population.setBreed(EnumBeeBreed.TEMPERATE);
                break;
        }

        swarm.setBeePopulation(population);
        swarm.setPosition(pos.getX(), pos.getY(), pos.getZ());
        return swarm;
    }

    @Override
    protected void initEntityAI() {
        this.tasks.addTask(1, new EntityBeeSwarm.AIBeeAttack(this));
        this.targetTasks.addTask(1, new EntityBeeSwarm.AIBeeTarget<>(this, EntityPlayer.class));
    }

    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(20.0D);
        this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED)
            .setBaseValue(0.30000001192092896D);
    }

    @Nullable
    @Override
    public IEntityLivingData onInitialSpawn(DifficultyInstance difficulty,
        @Nullable IEntityLivingData livingData) {
        livingData = super.onInitialSpawn(difficulty, livingData);

        if (livingData == null) {
            livingData = new EntityBeeSwarm.GroupData();
            ((EntityBeeSwarm.GroupData) livingData).setRandomPopulation();
            ((EntityBeeSwarm.GroupData) livingData).setPopulationBreed(this.getBreed());
            Habitus.LOG.info("Original: " + ((GroupData) livingData).population);
        }

        if (livingData instanceof EntityBeeSwarm.GroupData) {
            this.setBeePopulation(((GroupData) livingData).population);
            Habitus.LOG.info("Copy: " + ((GroupData) livingData).population);
        }

        return livingData;
    }

    protected abstract EnumBeeBreed getBreed();

    @Override
    public int getMaxSpawnedInChunk() {
        return 1;
    }

    public void setBeePopulation(BeePopulation population) {
        float newRadius =
            ((float) (population.getPopulationCount()) / (float) BeePopulation.MAXIMUM_POPULATION)
                * MAXIMUM_RADIUS;
        this.setRadius(newRadius);

        if (!this.world.isRemote) {
            this.dataManager.set(BEE_POPULATION, population.serializeToNBT());
        }
        this.breed = population.getBreed();
    }

    public BeePopulation getBeePopulation() {
        BeePopulation population = new BeePopulation();
        population.readFromNBT(dataManager.get(BEE_POPULATION));
        return population;
    }

    @Override
    @Nonnull
    public ITextComponent getDisplayName() {
        return new TextComponentString("Bees: " + this.getBeePopulation().getPopulationCount());
    }

    @Override
    @Nonnull
    public String getCustomNameTag() {
        return "Bees: " + this.getBeePopulation();
    }

    @Override
    public boolean getAlwaysRenderNameTag() {
        return false;
    }

    @Override
    public boolean hasCustomName() {
        return true;
    }

    @Override
    public void onUpdate() {
        if(this.world.isRemote) {
            if(color != getBeePopulation().getTemperament().getColorForPlayer(Minecraft.getMinecraft().player))
                color = getBeePopulation().getTemperament().getColorForPlayer(Minecraft.getMinecraft().player);

            if (this.isPartying) {
                color = new Color(Color.ColorType.RGB, rand.nextInt(256), rand.nextInt(256), rand.nextInt(256));

                EntityPlayer p = Minecraft.getMinecraft().player;

                float radius = this.getRadius() * 1.25f;
                for (int i = 0; i < 10; i++) {
                    double oX = ((rand.nextDouble() - 0.5) * radius);
                    double oY = ((rand.nextDouble() - 0.5) * this.height * 4);
                    double oZ = ((rand.nextDouble() - 0.5) * radius);

                    ParticleBee bee = new ParticleBee(this.world, this.posX + oX, this.posY + oY,
                            this.posZ + oZ, color.getRedFractional(), color.getGreenFractional(), color.getBlueFractional(), EnumBeeBreed.TEMPERATE).setDancing(true);
                    Minecraft.getMinecraft().effectRenderer.addEffect(bee);
                }
            } else {
                super.onUpdate();
            }
        }
    }

    @Override
    protected float getSoundVolume() {
        return isPartying ? 0.0f : super.getSoundVolume();
    }

    @Override
    protected void collideWithEntity(Entity entityIn) {
        super.collideWithEntity(entityIn);
        if(!entityIn.world.isRemote && entityIn instanceof EntityLivingBase) {
            EntityLivingBase living = (EntityLivingBase) entityIn;
            this.breed.applyEffect(living);
        }
    }

    @Override
    protected void entityInit() {
        super.entityInit();
        this.dataManager.register(BEE_POPULATION, new BeePopulation().serializeToNBT());
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound compound) {
        super.readEntityFromNBT(compound);
        BeePopulation population = new BeePopulation();
        population.readFromNBT(compound.getCompoundTag("BeePopulation"));
        this.setBeePopulation(population);
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound compound) {
        super.writeEntityToNBT(compound);
        compound.setTag("BeePopulation", this.dataManager.get(BEE_POPULATION));
    }

    @Override
    public void notifyDataManagerChange(@Nullable DataParameter<?> key) {
        if (BEE_POPULATION.equals(key)) {
            this.setBeePopulation(this.getBeePopulation());
        }

        super.notifyDataManagerChange(key);
    }

    static class AIBeeAttack extends EntityAIAttackMelee {

        AIBeeAttack(EntityBeeSwarm swarm) {
            super(swarm, 1.0D, true);
        }

        @Override
        public boolean shouldContinueExecuting() {
            float f = this.attacker.getBrightness();

            if (f >= 0.5F && this.attacker.getRNG().nextInt(100) == 0) {
                this.attacker.setAttackTarget(null);
                return false;
            } else {
                return super.shouldContinueExecuting();
            }
        }

        @Override
        protected double getAttackReachSqr(EntityLivingBase attackTarget) {
            return (double) (4.0F + attackTarget.width);
        }
    }

    static class AIBeeTarget<T extends EntityLivingBase> extends
        EntityAINearestAttackableTarget<T> {

        AIBeeTarget(EntityCreature creature, Class<T> classTarget) {
            super(creature, classTarget, true);
        }

        @Override
        public boolean shouldExecute() {
            if (this.taskOwner instanceof EntityBeeSwarm) {
                EntityBeeSwarm swarm = (EntityBeeSwarm) taskOwner;
                EnumBeeTemperament temperament = swarm.getBeePopulation().getTemperament();

                switch (temperament) {
                    case PEACEFUL:
                    case DOCILE:
                        return false;
                    case NEUTRAL:
                        return (swarm.rand.nextInt(100) < 1);
                    case AGGRESSIVE:
                        return super.shouldExecute();
                    case HOSTILE:
                        return super.shouldExecute();
                }
            }

            return false;
        }
    }

    @Override
    public void setPartying(BlockPos pos, boolean partying) {
        this.isPartying = partying;
    }

    @Override
    public String getName() {
        if (this.hasCustomName())
        {
            return this.getCustomNameTag();
        } else {
            return I18n.format("entity.habitus.bee_swarm.name", I18n.format(getBreed().name));
        }
    }



    static class GroupData implements IEntityLivingData {

        BeePopulation population;

        void setRandomPopulation() {
            this.population = BeePopulation.generateRandomPopulation();
            this.population.giveRandomSwarmPhenotype();
        }

        void setPopulationBreed(EnumBeeBreed breed) {
            this.population.setBreed(breed);
        }
    }

    public static class EntityTemperateBeeSwarm extends EntityBeeSwarm {

        public EntityTemperateBeeSwarm(World worldIn) {
            super(worldIn);
        }

        @Override
        public EnumBeeBreed getBreed() {
            return EnumBeeBreed.TEMPERATE;
        }
    }

    public static class EntityFrigidBeeSwarm extends EntityBeeSwarm {

        public EntityFrigidBeeSwarm(World worldIn) {
            super(worldIn);
        }

        @Override
        public EnumBeeBreed getBreed() {
            return EnumBeeBreed.FRIGID;
        }
    }

    public static class EntityAridBeeSwarm extends EntityBeeSwarm {

        public EntityAridBeeSwarm(World worldIn) {
            super(worldIn);
        }

        @Override
        public EnumBeeBreed getBreed() {
            return EnumBeeBreed.ARID;
        }
    }

    public static class EntitySwampyBeeSwarm extends EntityBeeSwarm {

        public EntitySwampyBeeSwarm(World worldIn) {
            super(worldIn);
        }

        @Override
        public EnumBeeBreed getBreed() {
            return EnumBeeBreed.SWAMPY;
        }
    }

    public static class EntityHellishBeeSwarm extends EntityBeeSwarm {

        public EntityHellishBeeSwarm(World worldIn) {
            super(worldIn);
        }

        @Override
        public EnumBeeBreed getBreed() {
            return EnumBeeBreed.HELLISH;
        }
    }

    public static class EntityFungalBeeSwarm extends EntityBeeSwarm {

        public EntityFungalBeeSwarm(World worldIn) {
            super(worldIn);
        }

        @Override
        protected EnumBeeBreed getBreed() {
            return EnumBeeBreed.FUNGAL;
        }
    }

    public static class EntityCeramicBeeSwarm extends EntityBeeSwarm {

        public EntityCeramicBeeSwarm(World worldIn) {
            super(worldIn);
        }

        @Override
        public EnumBeeBreed getBreed() {
            return EnumBeeBreed.CERAMIC;
        }
    }

    public static class EntityTranscendentBeeSwarm extends EntityBeeSwarm {

        public EntityTranscendentBeeSwarm(World worldIn) {
            super(worldIn);
        }

        @Override
        public EnumBeeBreed getBreed() {
            return EnumBeeBreed.TRANSCENDENT;
        }
    }

    public static class EntityHumidBeeSwarm extends EntityBeeSwarm {

        public EntityHumidBeeSwarm(World worldIn) {
            super(worldIn);
        }

        @Override
        public EnumBeeBreed getBreed() {
            return EnumBeeBreed.HUMID;
        }
    }

    public static class EntityWitheredBeeSwarm extends EntityBeeSwarm {

        public EntityWitheredBeeSwarm(World worldIn) {
            super(worldIn);
        }

        @Override
        public EnumBeeBreed getBreed() {
            return EnumBeeBreed.WITHERED;
        }
    }

}
