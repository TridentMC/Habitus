/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.entity;

import com.tridevmc.habitus.generic.Color;
import javax.annotation.Nonnull;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.util.IStringSerializable;

public enum EnumBeeTemperament implements IStringSerializable {
    PEACEFUL("habitus.temperament.peaceful", 0, 0x00FFFF, 0),
    DOCILE("habitus.temperament.docile", 1, 0x00FF00, 0),
    NEUTRAL("habitus.temperament.neutral", 2, 0xFFFF00, 0),
    AGGRESSIVE("habitus.temperament.aggressive", 3, 0xAA0000, 2),
    HOSTILE("habitus.temperament.hostile", 4, 0xFF0000, 5);

    final String name;
    final int id;
    final int color;
    final Color colorObj;
    final int attackRange;

    static Color defaultColor = new Color(Color.ColorType.RGB, 0xFFFF00);

    EnumBeeTemperament(String name, int id, int color, int attackRange) {
        this.name = name;
        this.id = id;
        this.color = color;
        this.colorObj = new Color(Color.ColorType.RGB, this.color);
        this.attackRange = attackRange;
    }

    public int getId() {
        return this.id;
    }

    public int getColor() {
        return this.color;
    }

    public Color getColorForPlayer(EntityPlayer player) {
        if(player.inventory.armorInventory.get(3).isEmpty() ||
                !player.inventory.armorInventory.get(3).getItem().equals(Items.IRON_HELMET)) {
            return defaultColor;
        } else {
            return this.colorObj;
        }
    }

    public Color getColorObj() {
        return this.colorObj;
    }

    public int getAttackRange() {
        return this.attackRange;
    }

    public static EnumBeeTemperament fromInt(int id) {
        switch (id) {
            case 0:
                return PEACEFUL;
            case 1:
                return DOCILE;
            case 2:
                return NEUTRAL;
            case 3:
                return AGGRESSIVE;
            case 4:
                return HOSTILE;
            default:
                return NEUTRAL;
        }
    }

    @Override
    @Nonnull
    public String getName() {
        return this.name;
    }

}
