/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.entity;

import javax.annotation.Nonnull;

import net.minecraft.client.resources.I18n;
import net.minecraft.util.IStringSerializable;

public enum     EnumBeeHealth implements IStringSerializable {
    HEALTHY("habitus.disease.healthy", 0),
    FOULBROOD("habitus.disease.foulbrood", 1),
    VAROOSIS("habitus.disease.varoosis", 2),
    NOSEMA("habitus.disease.nosema", 3),
    ACARINE("habitus.disease.acarine", 4),
    CHALKBROOD("habitus.disease.chalkbrood", 5);

    final String name;
    final int id;

    EnumBeeHealth(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public static EnumBeeHealth fromInt(int id) {
        switch (id) {
            case 0:
                return HEALTHY;
            case 1:
                return FOULBROOD;
            case 2:
                return VAROOSIS;
            case 3:
                return NOSEMA;
            case 4:
                return ACARINE;
            case 5:
                return CHALKBROOD;
            default:
                return HEALTHY;
        }
    }

    @Override
    @Nonnull
    public String getName() {
        return this.name;
    }

    public int getId() {
        return this.id;
    }
}
