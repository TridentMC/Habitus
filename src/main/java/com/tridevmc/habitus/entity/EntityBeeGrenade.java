/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.entity;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class EntityBeeGrenade extends EntityThrowable {

    public EntityBeeGrenade(World worldIn) {
        super(worldIn);
    }

    public EntityBeeGrenade(World worldIn, EntityPlayer playerIn) {
        super(worldIn, playerIn);
    }

    @Override
    protected void onImpact(RayTraceResult result) {
        if (!world.isRemote)
        {
            EntityDroneSwarm swarm = new EntityDroneSwarm(world);
            if(result.entityHit == null) {
                swarm.setLocationAndAngles(this.posX, this.posY, this.posZ, this.rotationYaw, 0.0F);
            } else {
                swarm.setPosition(result.entityHit.posX, result.entityHit.posY + 0.1, result.entityHit.posZ);
            }

            world.spawnEntity(swarm);
            world.setEntityState(this, (byte)3);
            setDead();
        }
    }
}
