/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.entity;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAttackMelee;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

public class EntityDroneSwarm extends EntityBaseSwarm {

    public EntityDroneSwarm(World worldIn) {
        super(worldIn);
        color = EnumBeeTemperament.defaultColor;
    }

    @Override
    protected void initEntityAI() {
        this.tasks.addTask(1, new AIDroneAttack(this));
        this.targetTasks.addTask(1, new AIDroneTarget<>(this, EntityPlayer.class));
    }

    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(20.0D);
        this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED)
                .setBaseValue(0.40000001192092896D);
    }

    @Override
    protected float getDamageAmount() {
        return 1.0f;
    }

    static class AIDroneAttack extends EntityAIAttackMelee {

        AIDroneAttack(EntityDroneSwarm swarm) {
            super(swarm, 1.0D, true);
        }

        @Override
        public boolean shouldContinueExecuting() {
            float f = this.attacker.getBrightness();

            if (f >= 0.5F && this.attacker.getRNG().nextInt(100) == 0) {
                this.attacker.setAttackTarget(null);
                return false;
            } else {
                return super.shouldContinueExecuting();
            }
        }

        @Override
        protected double getAttackReachSqr(EntityLivingBase attackTarget) {
            return (double) (4.0F + attackTarget.width);
        }
    }

    static class AIDroneTarget<T extends EntityLivingBase> extends
            EntityAINearestAttackableTarget<T> {

        AIDroneTarget(EntityCreature creature, Class<T> classTarget) {
            super(creature, classTarget, true);
        }

        @Override
        public boolean shouldExecute() {
            return super.shouldExecute();
        }
    }
}
