/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.entity;

import java.util.HashSet;
import java.util.Set;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.common.BiomeDictionary;

public class BiomeTools {

    public static class BiomeListBuilder {

        private final Set<Biome> workingSet;

        BiomeListBuilder() {
            workingSet = new HashSet<>();
        }

        public static BiomeListBuilder create() {
            return new BiomeListBuilder();
        }

        public BiomeListBuilder andIs(BiomeDictionary.Type type) {
            workingSet.addAll(BiomeDictionary.getBiomes(type));
            return this;
        }

        public BiomeListBuilder andCanBe(Biome biome) {
            workingSet.add(biome);
            return this;
        }

        public BiomeListBuilder andIsOnly(BiomeDictionary.Type type) {
            workingSet.retainAll(BiomeDictionary.getBiomes(type));
            return this;
        }

        public BiomeListBuilder andIsNot(BiomeDictionary.Type type) {
            workingSet.removeAll(BiomeDictionary.getBiomes(type));
            return this;
        }

        public Set<Biome> build() {
            return this.workingSet;
        }

    }
}
