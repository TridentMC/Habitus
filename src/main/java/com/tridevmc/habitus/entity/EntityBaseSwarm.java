/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.entity;

import com.tridevmc.habitus.generic.Color;
import com.tridevmc.habitus.init.HSSounds;
import com.tridevmc.habitus.particle.ParticleBee;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.pathfinding.PathNodeType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.World;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class EntityBaseSwarm extends EntityCreature {

    protected Color color;

    protected static final DataParameter<Float> RADIUS = EntityDataManager
            .createKey(EntityBeeSwarm.class, DataSerializers.FLOAT);
    protected static final float MAXIMUM_RADIUS = 3.0f;


    public EntityBaseSwarm(World worldIn) {
        super(worldIn);
        this.setPathPriority(PathNodeType.WATER, -1.0F);
    }

    @Override
    public void onUpdate() {
        super.onUpdate();
        if (this.world.isRemote) {
            EntityPlayer p = Minecraft.getMinecraft().player;

            float radius = this.getRadius() * 2;
            for (int i = 0; i < 10; i++) {
                double oX = ((rand.nextDouble() - 0.5) * radius);
                double oY = ((rand.nextDouble() - 0.5) * this.height * 2);
                double oZ = ((rand.nextDouble() - 0.5) * radius);

                ParticleBee bee = new ParticleBee(this.world, this.posX + oX, this.posY + oY,
                        this.posZ + oZ, color.getRedFractional(), color.getGreenFractional(), color.getBlueFractional(), EnumBeeBreed.TEMPERATE);
                Minecraft.getMinecraft().effectRenderer.addEffect(bee);
            }
        }
    }

    protected void setRadius(float newRadius) {
        double x = this.posX;
        double y = this.posY;
        double z = this.posZ;
        this.setSize(0.9f, 0.9f);
        this.setPosition(x, y, z);

        if (!this.world.isRemote) {
            this.dataManager.set(RADIUS, newRadius);
        }
    }

    protected float getRadius() {
        return this.dataManager.get(RADIUS);
    }

    @Override
    public boolean canBeAttackedWithItem() {
        return false;
    }

    @Override
    public boolean canBePushed() {
        return false;
    }

    @Override
    public boolean canBeHitWithPotion() {
        return false;
    }

    @Override
    public boolean attackEntityFrom(@Nonnull DamageSource source, float amount) {
        if (source == DamageSource.IN_WALL) {
            this.noClip = true;
            this.move(MoverType.SELF, 0, 1, 0);
            this.noClip = false;
            return false;
        } else {
            return super.attackEntityFrom(source, amount);
        }
    }

    @Override
    protected void entityInit() {
        super.entityInit();
        this.dataManager.register(RADIUS, 0.5f);
    }


    @Override
    protected void collideWithEntity(Entity entityIn) {
        if(!entityIn.world.isRemote && entityIn instanceof EntityLivingBase) {
            EntityLivingBase living = (EntityLivingBase) entityIn;
            living.attackEntityFrom(DamageSource.GENERIC, getDamageAmount());
        }
    }

    protected float getDamageAmount() {
        return 0.5f;
    }

    @Nullable
    @Override
    protected SoundEvent getAmbientSound() {
        return HSSounds.bee_swarm_idle;
    }

    @Override
    public int getTalkInterval() {
        return 0;
    }

    @Override
    public void notifyDataManagerChange(@Nullable DataParameter<?> key) {
        if (RADIUS.equals(key)) {
            this.setRadius(this.getRadius());
        }

        super.notifyDataManagerChange(key);
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound compound) {
        this.setRadius(compound.getFloat("Radius"));
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound compound) {
        compound.setFloat("Radius", this.getRadius());
    }

}
