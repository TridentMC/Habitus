/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import net.minecraft.client.resources.I18n;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagInt;
import net.minecraft.nbt.NBTTagList;
import net.minecraftforge.common.util.Constants;

public class BeePhenotype {

    // A colony's resistances are all the possible diseases that it has a _reduced_
    // chance of suffering.
    private List<EnumBeeHealth> resistances;

    // A colony's immunities are all the possible diseases that it _cannot_ suffer,
    // no matter what.
    private List<EnumBeeHealth> immunities;

    // A colony's susceptibilities are all the possible diseases that it has an _increased_
    // chance of suffering.
    private List<EnumBeeHealth> susceptibilities;

    // An integer factor that determines the approximate base rate of growth of the colony.
    private int growthFactor;

    // An integer factor that determines the approximate base rate of decay of the colony.
    private int decayFactor;

    public BeePhenotype() {
        this.resistances = new ArrayList<>();
        this.immunities = new ArrayList<>();
        this.susceptibilities = new ArrayList<>();
    }

    public void giveResistance(EnumBeeHealth disease) {
        if (disease == EnumBeeHealth.HEALTHY || resistances.contains(disease) || immunities
            .contains(disease)) {
            return;
        }

        if (susceptibilities.contains(disease)) {
            susceptibilities.remove(disease);
        }

        resistances.add(disease);
    }

    public void giveImmunity(EnumBeeHealth disease) {
        if (disease == EnumBeeHealth.HEALTHY || immunities.contains(disease)) {
            return;
        }

        if (susceptibilities.contains(disease)) {
            susceptibilities.remove(disease);
        }

        if (resistances.contains(disease)) {
            resistances.remove(disease);
        }

        immunities.add(disease);
    }

    public void giveSusceptibility(EnumBeeHealth disease) {
        if (disease == EnumBeeHealth.HEALTHY || immunities.contains(disease)) {
            return;
        }

        if (resistances.contains(disease)) {
            resistances.remove(disease);
        }

        susceptibilities.add(disease);
    }

    public List<EnumBeeHealth> getResistances() {
        return this.resistances;
    }

    public List<EnumBeeHealth> getImmunities() {
        return this.immunities;
    }

    public List<EnumBeeHealth> getSusceptibilities() {
        return this.susceptibilities;
    }

    private int breedSusceptibilitiesChance(EnumBeeHealth s, List<EnumBeeHealth> sl,
        List<EnumBeeHealth> il) {
        int chance = 10;
        if (sl.contains(s)) {
            chance = 30;
        } else if (il.contains(s)) {
            chance = 5;
        }

        return chance;
    }

    private int breedResistancesChance(EnumBeeHealth s, List<EnumBeeHealth> rl,
        List<EnumBeeHealth> il) {
        int chance = 15;
        if (rl.contains(s)) {
            chance = 40;
        } else if (il.contains(s)) {
            chance = 25;
        }

        return chance;
    }

    private int breedImmunitiesChance(EnumBeeHealth s, List<EnumBeeHealth> il) {
        int chance = 10;
        if (il.contains(s)) {
            chance = 25;
        }

        return chance;
    }

    public BeePhenotype breed(BeePhenotype other) {
        BeePhenotype phenotype = new BeePhenotype();
        Random rand = new Random();
        List<EnumBeeHealth> resistances = other.getResistances();
        for (EnumBeeHealth resistance : resistances) {
            int chance = breedSusceptibilitiesChance(resistance, this.resistances, this.immunities);
            if (rand.nextInt(100) < chance) {
                phenotype.giveResistance(resistance);
            }
        }

        for (EnumBeeHealth resistance : this.resistances) {
            int chance = breedSusceptibilitiesChance(resistance, other.getResistances(),
                other.getImmunities());
            if (rand.nextInt(100) < chance) {
                phenotype.giveResistance(resistance);
            }
        }

        List<EnumBeeHealth> susceptibilities = other.getSusceptibilities();
        for (EnumBeeHealth susceptibility : susceptibilities) {
            int chance = breedSusceptibilitiesChance(susceptibility, this.susceptibilities,
                this.immunities);
            if (rand.nextInt(100) < chance) {
                phenotype.giveSusceptibility(susceptibility);
            }
        }

        for (EnumBeeHealth susceptibility : this.susceptibilities) {
            int chance = breedSusceptibilitiesChance(susceptibility, other.getSusceptibilities(),
                other.getImmunities());
            if (rand.nextInt(100) < chance) {
                phenotype.giveSusceptibility(susceptibility);
            }
        }

        List<EnumBeeHealth> immunities = other.getImmunities();
        for (EnumBeeHealth immunity : immunities) {
            int chance = breedImmunitiesChance(immunity, this.immunities);
            if (rand.nextInt(100) < chance) {
                phenotype.giveImmunity(immunity);
            }
        }

        for (EnumBeeHealth immunity : this.immunities) {
            int chance = breedImmunitiesChance(immunity, other.getImmunities());
            if (rand.nextInt(100) < chance) {
                phenotype.giveImmunity(immunity);
            }
        }

        return phenotype;

    }

    public int getChanceOfDisease(EnumBeeHealth disease) {
        if (disease == EnumBeeHealth.HEALTHY) {
            return 0;
        }

        if (this.immunities.contains(disease)) {
            return 0;
        } else if (this.resistances.contains(disease)) {
            return 15;
        } else if (this.susceptibilities.contains(disease)) {
            return 55;
        } else {
            return 25;
        }
    }

    public void deserializeFromNBT(NBTTagCompound compound) {
        NBTTagList sus = compound.getTagList("susceptibilities", Constants.NBT.TAG_INT);
        for (NBTBase i : sus) {
            this.giveSusceptibility(EnumBeeHealth.fromInt(((NBTTagInt) i).getInt()));
        }

        NBTTagList res = compound.getTagList("resistances", Constants.NBT.TAG_INT);
        for (NBTBase i : res) {
            this.giveResistance(EnumBeeHealth.fromInt(((NBTTagInt) i).getInt()));
        }

        NBTTagList imm = compound.getTagList("immunities", Constants.NBT.TAG_INT);
        for (NBTBase i : imm) {
            this.giveImmunity(EnumBeeHealth.fromInt(((NBTTagInt) i).getInt()));
        }
    }

    public NBTTagCompound serializeToNBT() {
        NBTTagCompound compound = new NBTTagCompound();
        NBTTagList imm = new NBTTagList();
        for (EnumBeeHealth immunity : immunities) {
            imm.appendTag(new NBTTagInt(immunity.getId()));
        }

        NBTTagList res = new NBTTagList();
        for (EnumBeeHealth resistance : resistances) {
            res.appendTag(new NBTTagInt(resistance.getId()));
        }

        NBTTagList sus = new NBTTagList();
        for (EnumBeeHealth susceptibility : susceptibilities) {
            sus.appendTag(new NBTTagInt(susceptibility.getId()));
        }

        compound.setTag("immunities", imm);
        compound.setTag("resistances", res);
        compound.setTag("susceptibilities", sus);

        return compound;
    }

    private String phenotytpeStringBuilder(List<EnumBeeHealth> list) {
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            res.append(I18n.format(list.get(i).getName())).append(i == list.size() - 1 ? "" : ", ");
        }
        return res.toString();
    }

    public String getSusceptibilitiesString() {
        return phenotytpeStringBuilder(this.susceptibilities);
    }

    public String getResistancesString() {
        return phenotytpeStringBuilder(this.resistances);
    }

    public String getImmunitiesString() {
        return phenotytpeStringBuilder(this.immunities);
    }
}
