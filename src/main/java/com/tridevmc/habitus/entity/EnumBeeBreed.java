/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.entity;

import javax.annotation.Nonnull;

import net.minecraft.client.resources.I18n;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.init.MobEffects;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.IStringSerializable;

import java.util.function.Consumer;

public enum EnumBeeBreed implements IStringSerializable {
    TEMPERATE("habitus.breed.temperate", 0, 0x61ef1f, null),
    FRIGID("habitus.breed.frigid", 1, 0x22f4c0, createPotionEffectConsumer(new PotionEffect(MobEffects.SLOWNESS, 5*20, 0))),
    ARID("habitus.breed.arid", 2, 0xf5ff87, createPotionEffectConsumer(new PotionEffect(MobEffects.HUNGER, 10*20, 0))),
    SWAMPY("habitus.breed.swampy", 3, 0x8dba46, createPotionEffectConsumer(new PotionEffect(MobEffects.POISON, 5*20, 0))),
    HELLISH("habitus.breed.hellish", 4, 0xaf1c15, (ent) -> {ent.setFire(5);}),
    FUNGAL("habitus.breed.fungal", 5, 0x5e3434, null),
    CERAMIC("habitus.breed.ceramic", 6, 0xd6a48f, null),
    TRANSCENDENT("habitus.breed.transcendent", 7, 0x9826c9, (ent) -> {/*TODO: Ender Teleportation*/}),
    HUMID("habitus.breed.humid", 8, 0x093000, createPotionEffectConsumer(new PotionEffect(MobEffects.POISON, 5*20, 1))),
    WITHERED("habitus.breed.withered",9, 0x34373a, createPotionEffectConsumer(new PotionEffect(MobEffects.WITHER, 5*20, 0)));


    final String name;
    final int number;
    final int color;
    final Consumer<EntityLivingBase> effect;

    private static Consumer<EntityLivingBase> createPotionEffectConsumer(PotionEffect e) {
        return (EntityLivingBase ent) -> {
            ent.addPotionEffect(new PotionEffect(e));
        };
    }

    EnumBeeBreed(String name, int number, int color, Consumer<EntityLivingBase> effect) {
        this.name = name;
        this.number = number;
        this.color = color;
        this.effect = effect;
    }

    public static final int BREED_COUNT = 10;


    @Override
    @Nonnull
    public String getName() {
        return name;
    }

    public int getId() {
        return this.number;
    }

    public int getColor() {
        return this.color;
    }

    public void applyEffect(@Nonnull EntityLivingBase ent) {
        if(this.effect != null)
            this.effect.accept(ent);
    }

    public static EnumBeeBreed fromInt(int id) {
        switch (id) {
            case 0:
                return TEMPERATE;
            case 1:
                return FRIGID;
            case 2:
                return ARID;
            case 3:
                return SWAMPY;
            case 4:
                return HELLISH;
            case 5:
                return FUNGAL;
            case 6:
                return CERAMIC;
            case 7:
                return TRANSCENDENT;
            case 8:
                return HUMID;
            case 9:
                return WITHERED;
            default:
                return TEMPERATE;
        }
    }
}
