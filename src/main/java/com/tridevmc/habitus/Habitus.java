/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus;

import com.elytradev.probe.api.IProbeDataProvider;
import com.tridevmc.habitus.gui.creative.HabitusCreativeTab;
import com.tridevmc.habitus.init.HSBlocks;
import com.tridevmc.habitus.init.HSEntities;
import com.tridevmc.habitus.init.HSFluids;
import com.tridevmc.habitus.init.HSItems;
import com.tridevmc.habitus.init.HSSounds;
import com.tridevmc.habitus.proxy.CommonProxy;
import com.tridevmc.habitus.world.WorldGen;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.storage.loot.LootTableList;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.terraingen.InitMapGenEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.EntityEntry;
import net.minecraftforge.fml.common.registry.GameRegistry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@SuppressWarnings("EmptyMethod")
@Mod(
    modid = Habitus.MOD_ID,
    name = Habitus.MOD_NAME,
    version = Habitus.MOD_VERSION)
@Mod.EventBusSubscriber
public class Habitus {

    public static final String MOD_ID = "habitus";
    @SuppressWarnings("WeakerAccess")
    public static final String MOD_NAME = "Habitus";
    @SuppressWarnings("WeakerAccess")
    public static final String MOD_VERSION = "1.12.2-0.1.0";

    public static final HabitusCreativeTab habitusTab = new HabitusCreativeTab(MOD_ID);

    public static ResourceLocation helletonLootTable;

    @Mod.Instance(MOD_ID)
    public static Habitus instance;

    @SidedProxy(
        clientSide = "com.tridevmc.habitus.proxy.ClientProxy",
        serverSide = "com.tridevmc.habitus.proxy.CommonProxy")
    public static CommonProxy proxy;

    public static final Logger LOG = LogManager.getLogger(MOD_NAME);

    @CapabilityInject(IProbeDataProvider.class)
    public static Capability<IProbeDataProvider> PROBE_CAPABILITY = null;

    static {
        net.minecraftforge.fluids.FluidRegistry.enableUniversalBucket();
        net.minecraftforge.fluids.FluidRegistry
            .addBucketForFluid(net.minecraftforge.fluids.FluidRegistry.WATER);
        net.minecraftforge.fluids.FluidRegistry
            .addBucketForFluid(net.minecraftforge.fluids.FluidRegistry.LAVA);
    }

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        System.out.println(MOD_NAME + " version " + MOD_VERSION + " is starting up!");
        HSFluids.init();
        proxy.init();
        GameRegistry.registerWorldGenerator(new WorldGen(), 3);
        helletonLootTable = LootTableList
            .register(new ResourceLocation("habitus", "entities/helleton"));
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        MinecraftForge.addGrassSeed(new ItemStack(HSItems.barleySeeds, 1, 0), 16);
        FurnaceRecipes.instance()
            .addSmeltingRecipeForBlock(HSBlocks.copperOre, new ItemStack(HSItems.copperIngot),
                0.7f);
        FurnaceRecipes.instance()
            .addSmeltingRecipeForBlock(HSBlocks.tinOre, new ItemStack(HSItems.tinIngot), 0.7f);

    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {

    }

    @SubscribeEvent
    public static void registerBlocks(RegistryEvent.Register<Block> event) {
        HSBlocks.registerBlocks(event.getRegistry());
    }

    @SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> event) {
        HSItems.registerItems(event.getRegistry());
        HSBlocks.registerItemBlocks(event.getRegistry());
    }

    @SubscribeEvent
    public static void registerModels(ModelRegistryEvent event) {
        HSBlocks.registerBlockModels();
        HSItems.registerItemModels();
    }

    @SubscribeEvent
    public static void registerSounds(RegistryEvent.Register<SoundEvent> event) {
        HSSounds.registerSounds(event.getRegistry());
    }

    @SubscribeEvent
    public static void registerEntities(RegistryEvent.Register<EntityEntry> event) {
        HSEntities.registerEntities(event.getRegistry());
    }

    @SubscribeEvent
    public static void onInitMapGen(InitMapGenEvent event) {
        HSEntities.onInitMapGen(event);
    }
}
