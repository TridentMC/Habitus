/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.gui.container;

import com.elytradev.concrete.inventory.gui.widget.WFieldedLabel;
import com.elytradev.concrete.inventory.gui.widget.WItemSlot;
import com.elytradev.concrete.inventory.gui.widget.WPlainPanel;
import com.tridevmc.habitus.gui.ILockableInventory;
import com.tridevmc.habitus.gui.widget.WBeeBreedLabel;
import com.tridevmc.habitus.gui.widget.WBeeTemperamentLabel;
import com.tridevmc.habitus.gui.widget.WFixedPanel;
import com.tridevmc.habitus.gui.widget.WLockableSingleSlot;
import com.tridevmc.habitus.tile.TileEntityApiary;
import net.minecraft.inventory.IInventory;

public class ContainerApiary extends ContainerBase<TileEntityApiary> {

    public ContainerApiary(TileEntityApiary apiary, IInventory playerInventory,
        ILockableInventory apiaryInventory) {
        super(apiary, playerInventory, apiaryInventory);

        WFixedPanel panel = new WFixedPanel();
        this.setRootPanel(panel);

        panel.add(WLockableSingleSlot.lockableOf(apiaryInventory, 0), 18, (int) (1.75f * 18));
        panel.add(WLockableSingleSlot.lockableOf(apiaryInventory, 1), 4 * 18, (int) (1.75f * 18));
        panel.add(WLockableSingleSlot.lockableOf(apiaryInventory, 2), 7 * 18, (int) (1.75f * 18));
        panel.add(WItemSlot.ofPlayerStorage(playerInventory), 0, 84);
        panel.add(WItemSlot.of(playerInventory, 0, 9, 1), 0, 144);
        panel.add(
            new WFieldedLabel(apiaryInventory, TileEntityApiary.FIELD_BEE_COUNT, "Bee Count: %f"),
            0, 50);
        panel.add(new WBeeTemperamentLabel(apiaryInventory, TileEntityApiary.FIELD_BEE_TEMPERAMENT,
            "Bee Temperament: ", 0), 0, 62);
        panel.add(
            new WBeeBreedLabel(apiaryInventory, TileEntityApiary.FIELD_BEE_BREED, "Bee Breed: ", 0),
            0, 74);
    }
}