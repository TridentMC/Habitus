/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.gui.container;

import com.elytradev.concrete.inventory.gui.widget.WItemSlot;
import com.elytradev.concrete.inventory.gui.widget.WPlainPanel;
import com.tridevmc.habitus.gui.widget.WBeePopulationInformation;
import com.tridevmc.habitus.gui.widget.WChangeStateButton;
import com.tridevmc.habitus.gui.widget.WFixedPanel;
import com.tridevmc.habitus.tile.TileEntityBeeAnalyzer;
import net.minecraft.inventory.IContainerListener;
import net.minecraft.inventory.IInventory;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ContainerBeeAnalyzer extends ContainerBase<TileEntityBeeAnalyzer> implements
    IContainerWithState<ContainerBeeAnalyzer.BeeAnalyzerState> {

    private TileEntityBeeAnalyzer analyzer;
    private boolean wasAnalyzing;

    private BeeAnalyzerState state = BeeAnalyzerState.WAITING;

    public ContainerBeeAnalyzer(TileEntityBeeAnalyzer beeAnalyzer, IInventory playerInventory,
        IInventory analyzerInventory) {
        super(beeAnalyzer, playerInventory, analyzerInventory);

        WFixedPanel panel = new WFixedPanel();
        this.setRootPanel(panel);
        analyzer = beeAnalyzer;

        if(beeAnalyzer.wasAnalyzed()) state = BeeAnalyzerState.BASIC;

        panel.add(WItemSlot.ofPlayerStorage(playerInventory), 0, 84);
        panel.add(WItemSlot.of(playerInventory, 0, 9, 1), 0, 144);
        panel.add(new WBeePopulationInformation(this, analyzerInventory, beeAnalyzer.getPopulation()), 0, 0);
        panel.add(new WChangeStateButton<>(this, BeeAnalyzerState.BASIC, "B")
                .disableOn(BeeAnalyzerState.WAITING)
                .disableOn(BeeAnalyzerState.ANALYZING), 80, -2);
        panel.add(new WChangeStateButton<>(this, BeeAnalyzerState.PHENOTYPE, "P")
                .disableOn(BeeAnalyzerState.WAITING)
                .disableOn(BeeAnalyzerState.ANALYZING), 100, -2);
    }

    @Override
    public BeeAnalyzerState getCurrentState() {
        return state;
    }

    @Override
    public void changeState(BeeAnalyzerState newState) {
        state = newState;
    }

    @Override
    public void detectAndSendChanges() {
        super.detectAndSendChanges();

        boolean dirty = false;

        if(!analyzer.getActive() && this.state != BeeAnalyzerState.WAITING) {
            this.state = BeeAnalyzerState.WAITING;
            dirty = true;
        }

        if(analyzer.isAnalyzing() && this.state != BeeAnalyzerState.ANALYZING)
        {
            this.state = BeeAnalyzerState.ANALYZING;
            dirty = true;
        } else if(!analyzer.isAnalyzing() && this.state == BeeAnalyzerState.ANALYZING) {
            this.state = BeeAnalyzerState.BASIC;
            dirty = true;
        }

        if(dirty) {
            for(IContainerListener listener : listeners) {
                listener.sendWindowProperty(this, TileEntityBeeAnalyzer.FIELD_CONTAINER_STATE, this.state.num);
            }
        }
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void updateProgressBar(int id, int data) {
        super.updateProgressBar(id, data);
        if(id == TileEntityBeeAnalyzer.FIELD_CONTAINER_STATE)
            this.state = BeeAnalyzerState.fromNum(data);
    }

    public enum BeeAnalyzerState {
        WAITING(0),
        ANALYZING(1),
        BASIC(2),
        PHENOTYPE(3);

        final int num;

        BeeAnalyzerState(int num) {
            this.num = num;
        }

        public int getNum() {
            return num;
        }

        public static BeeAnalyzerState fromNum(int num) {
            switch(num) {
                case 0:
                    return WAITING;
                case 1:
                    return ANALYZING;
                case 2:
                    return BASIC;
                case 3:
                    return PHENOTYPE;
                default:
                    return WAITING;
            }
        }
    }

}
