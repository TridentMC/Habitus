/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.gui;

import com.elytradev.concrete.inventory.ConcreteItemStorage;
import com.elytradev.concrete.inventory.ValidatedInventoryView;
import com.tridevmc.habitus.tile.BeePopulation;
import java.util.function.Supplier;

public class AnalyzerInventoryView extends ValidatedInventoryView {

    private final Supplier<BeePopulation> populationDelegate;

    public AnalyzerInventoryView(ConcreteItemStorage delegate,
        Supplier<BeePopulation> populationDelegate) {
        super(delegate);
        this.populationDelegate = populationDelegate;
    }

    public BeePopulation getPopulation() {
        return this.populationDelegate.get();
    }


}
