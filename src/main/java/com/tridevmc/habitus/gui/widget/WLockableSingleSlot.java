package com.tridevmc.habitus.gui.widget;

import com.elytradev.concrete.inventory.gui.ConcreteContainer;
import com.tridevmc.habitus.gui.ILockableInventory;

public class WLockableSingleSlot extends WSingleItemSlot {
    public WLockableSingleSlot(ILockableInventory inventory, int startIndex, int slotsWide, int slotsHigh, boolean big, boolean ltr) {
        super(inventory, startIndex, slotsWide, slotsHigh, big, ltr);
    }

    public static WLockableSingleSlot lockableOf(ILockableInventory inventory, int index) {
        WLockableSingleSlot w = new WLockableSingleSlot();
        w.inventory = inventory;
        w.startIndex = index;
        return w;
    }

    private WLockableSingleSlot() {
    }

    @Override
    public void createPeers(ConcreteContainer c) {
        this.container = c;
        this.peers.clear();
        int index = this.startIndex;

        for (int y = 0; y < this.slotsHigh; ++y) {
            for (int x = 0; x < this.slotsWide; ++x) {
                LockableSingleSlot slot = new LockableSingleSlot((ILockableInventory)this.inventory, index,
                        this.getAbsoluteX() + x * 18, this.getAbsoluteY() + y * 18);
                this.peers.add(slot);
                c.addSlotPeer(slot);
                ++index;
            }
        }
    }
}
