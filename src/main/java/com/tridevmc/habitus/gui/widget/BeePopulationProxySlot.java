/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.gui.widget;

import com.tridevmc.habitus.tile.BeePopulation;
import javax.annotation.Nonnull;
import net.minecraft.init.Items;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;

public class BeePopulationProxySlot extends Slot {

    private final BeePopulation population;

    public BeePopulationProxySlot(BeePopulation population) {
        super(null, 0, -2147483648, -2147483648);
        this.population = population;
    }

    @Nonnull
    public ItemStack getStack() {
        NBTTagCompound populationTag = new NBTTagCompound();
        NBTTagCompound population = new NBTTagCompound();
        NBTTagCompound display = new NBTTagCompound();
        NBTTagList garbageLore = new NBTTagList();
        garbageLore.appendTag(new NBTTagString("What? How can you see this?"));
        garbageLore.appendTag(new NBTTagString("Tell @CalmBit immediately."));
        display.setTag("Name", new NBTTagString("Proxy Stick"));
        display.setTag("Lore", garbageLore);
        this.population.writeToNBT(population);
        populationTag.setTag("population", population);
        populationTag.setTag("display", display);
        ItemStack result = new ItemStack(Items.STICK, 1, 0);
        result.setTagCompound(populationTag);
        return result;
    }

    public void putStack(@Nonnull ItemStack stack) {
        if (stack.getTagCompound() != null && stack.getTagCompound().hasKey("population")) {
            this.population.readFromNBT(stack.getTagCompound().getCompoundTag("population"));
        }

    }

    public void onSlotChanged() {
        //this.population.markDirty();
    }

    public int getSlotStackLimit() {
        return 1;
    }

    @Nonnull
    public ItemStack decrStackSize(int amount) {
        return ItemStack.EMPTY;
    }

    public boolean isHere(IInventory inv, int slotIn) {
        return false;
    }

    public boolean isSameInventory(Slot other) {
        if (other instanceof BeePopulationProxySlot) {
            BeePopulationProxySlot slot = (BeePopulationProxySlot) other;
            if (slot.population == this.population) {
                return true;
            }
        }

        return false;
    }
}
