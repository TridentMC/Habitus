/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.gui.widget;

import com.elytradev.concrete.inventory.gui.ConcreteContainer;
import com.elytradev.concrete.inventory.gui.client.GuiDrawing;
import com.elytradev.concrete.inventory.gui.widget.WWidget;
import com.sun.imageio.plugins.common.I18N;
import com.tridevmc.habitus.entity.BeePhenotype;
import com.tridevmc.habitus.entity.EnumBeeBreed;
import com.tridevmc.habitus.entity.EnumBeeTemperament;
import com.tridevmc.habitus.gui.container.ContainerBeeAnalyzer;
import com.tridevmc.habitus.tile.BeePopulation;
import com.tridevmc.habitus.tile.TileEntityBeeAnalyzer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.resources.I18n;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.ResourceLocation;

public class WBeePopulationInformation extends WWidget {

    private final ContainerBeeAnalyzer parentContainer;
    private final BeePopulation population;
    private final IInventory analyzerInventory;
    private static final String countLabel = I18n.format("tile.habitus.bee_analyzer.label.size");
    private static final String temperamentLabel = I18n.format("tile.habitus.bee_analyzer.label.temperament");
    private static final String breedLabel = I18n.format("tile.habitus.bee_analyzer.label.breed");
    private static final int offsetX = 2;
    private static final int windowOffsetY = 12;
    private static final int countOffsetY = windowOffsetY + 4;
    private static final int temperamentOffsetY = countOffsetY + 12;
    private static final int breedOffsetY = temperamentOffsetY + 12;
    private static final int healthOffsetY = breedOffsetY + 12;
    private static final int susOffsetY = countOffsetY;
    private static final int resOffsetY = susOffsetY + 12;
    private static final int immOffsetY = resOffsetY + 12;
    private static final int analysisOffsetY = 25;
    private static final int analysisOffsetX = 80;

    public WBeePopulationInformation(ContainerBeeAnalyzer beeAnalyzer, IInventory analyzerInventory, BeePopulation population) {
        this.population = population;
        parentContainer = beeAnalyzer;
        this.analyzerInventory = analyzerInventory;
    }

    @Override
    public void createPeers(ConcreteContainer c) {
        c.addSlotPeer(new BeePopulationProxySlot(population));
    }

    @Override
    public void paintBackground(int x, int y) {

        GuiDrawing
            .rect(new ResourceLocation("habitus", "textures/gui/panel.png"), x, y + windowOffsetY,
                160, 60, -1);

        switch (parentContainer.getCurrentState()) {
            case WAITING: {
                String text = I18n.format("tile.habitus.bee_analyzer.label.waiting");
                GuiDrawing.drawString(text, x + analysisOffsetX - Minecraft.getMinecraft().fontRenderer.getStringWidth(text) / 2,
                        y + analysisOffsetY, 0xBFBFBF);
                break;
            }
            case ANALYZING: {
                String text = I18n.format("tile.habitus.bee_analyzer.label.analyzing",
                        analyzerInventory.getField(TileEntityBeeAnalyzer.FIELD_ANALYSIS_PROGRESS),
                        analyzerInventory.getField(TileEntityBeeAnalyzer.FIELD_ANALYSIS_MAX));
                GuiDrawing.drawString(text, x + analysisOffsetX - Minecraft.getMinecraft().fontRenderer.getStringWidth(text) / 2,
                        y + analysisOffsetY, 0xBFBFBF);
                break;
            }
            case BASIC: {
                GuiDrawing.drawString(countLabel, x + offsetX, y + countOffsetY, 0xBFBFBF);
                EnumBeeTemperament temperament = population.getTemperament();
                GuiDrawing
                    .drawString(temperamentLabel, x + offsetX, y + temperamentOffsetY, 0xBFBFBF);
                EnumBeeBreed breed = population.getBreed();
                GuiDrawing.drawString(breedLabel, x + offsetX, y + breedOffsetY, 0xBFBFBF);
                if (!population.hasActivePopulation()) {
                    GuiDrawing.drawString("0",
                        x + offsetX + Minecraft.getMinecraft().getRenderManager().getFontRenderer()
                            .getStringWidth(countLabel), y + countOffsetY, 0x9A9A9A);
                    GuiDrawing.drawString("N/A",
                        x + offsetX + Minecraft.getMinecraft().getRenderManager().getFontRenderer()
                            .getStringWidth(temperamentLabel), y + temperamentOffsetY, 0x9A9A9A);
                    GuiDrawing.drawString("N/A",
                        x + offsetX + Minecraft.getMinecraft().getRenderManager().getFontRenderer()
                            .getStringWidth(breedLabel), y + breedOffsetY, 0x9A9A9A);
                } else {
                    GuiDrawing.drawString("" + population.getPopulationCount(),
                        x + offsetX + Minecraft.getMinecraft().getRenderManager().getFontRenderer()
                            .getStringWidth(countLabel), y + countOffsetY, 0x9A9A9A);
                    GuiDrawing.drawString(I18n.format(temperament.getName()),
                        x + offsetX + Minecraft.getMinecraft().getRenderManager().getFontRenderer()
                            .getStringWidth(temperamentLabel), y + temperamentOffsetY,
                        temperament.getColor());
                    GuiDrawing.drawString(I18n.format(breed.getName()),
                        x + offsetX + Minecraft.getMinecraft().getRenderManager().getFontRenderer()
                            .getStringWidth(breedLabel), y + breedOffsetY, breed.getColor());
                    GuiDrawing.drawString(I18n.format(population.getHealth().getName()), x + offsetX,
                        y + breedOffsetY + 12, 0xFFFFFF);
                }
                break;
            }
            case PHENOTYPE: {
                if (population.hasActivePopulation()) {
                    BeePhenotype phenotype = population.getPhenotype();
                    GuiDrawing.drawString(phenotype.getSusceptibilitiesString(), x + offsetX,
                        y + susOffsetY, 0xFF0000);
                    GuiDrawing
                        .drawString(phenotype.getResistancesString(), x + offsetX, y + resOffsetY,
                            0x00FF00);
                    GuiDrawing
                        .drawString(phenotype.getImmunitiesString(), x + offsetX, y + immOffsetY,
                            0x0000FF);
                }
                break;
            }
        }
    }
}
