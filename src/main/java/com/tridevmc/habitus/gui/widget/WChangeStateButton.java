/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.gui.widget;

import com.elytradev.concrete.inventory.gui.client.GuiDrawing;
import com.elytradev.concrete.inventory.gui.widget.WWidget;
import com.tridevmc.habitus.gui.container.IContainerWithState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.ResourceLocation;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class WChangeStateButton<T extends Enum> extends WWidget {

    public IContainerWithState<T> parentContainer;
    public T stateToUse;
    public Set<T> disabledStates = new HashSet<>();
    private boolean mouseDown = false;
    private String label;

    public WChangeStateButton(IContainerWithState<T> parentContainer, T stateToUse, String label) {
        this.parentContainer = parentContainer;
        this.stateToUse = stateToUse;
        this.label = label;
        this.setSize(18, 12);
    }

    @Override
    public void onClick(int x, int y, int button) {

        if(this.disabledStates.contains(this.parentContainer.getCurrentState()))
            return;

        if (this.parentContainer.getCurrentState() != stateToUse) {
            this.parentContainer.changeState(stateToUse);
        }

        Minecraft.getMinecraft().getSoundHandler()
            .playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_BUTTON_CLICK, 1.0F));
    }

    @Override
    public WWidget onMouseDown(int x, int y, int button) {
        this.mouseDown = true;
        return super.onMouseDown(x, y, button);
    }

    @Override
    public WWidget onMouseUp(int x, int y, int button) {
        this.mouseDown = false;
        return super.onMouseUp(x, y, button);
    }

    public WChangeStateButton<T> disableOn(T state) {
        this.disabledStates.add(state);
        return this;
    }

    @Override
    public void paintForeground(int x, int y, int mouseX, int mouseY) {
        if(this.disabledStates.contains(this.parentContainer.getCurrentState())) {
            GuiDrawing
                    .rect(new ResourceLocation("habitus", "textures/gui/button_disabled.png"), this.getX(),
                            this.getY(), getWidth(), getHeight(), 0xFFFFFF);
            GuiDrawing
                    .drawString(label, this.getX() + this.getCenterOffset(), this.getY() + 2, 0xA0A0A0);
        } else if (this.isHovering(x, y, mouseX, mouseY)) {
            GuiDrawing
                .rect(new ResourceLocation("habitus", "textures/gui/button_hover.png"), this.getX(),
                    this.getY(), getWidth(), getHeight(), 0xFFFFFF);
            GuiDrawing
                .drawString(label, this.getX() + this.getCenterOffset(), this.getY() + 2, 0xFFFFA0);
        } else {
            GuiDrawing.rect(new ResourceLocation("habitus", "textures/gui/button.png"), this.getX(),
                this.getY(), getWidth(), getHeight(), 0xFFFFFF);
            GuiDrawing
                .drawString(label, this.getX() + this.getCenterOffset(), this.getY() + 2, 0xE0E0E0);
        }
    }

    private boolean isHovering(int x, int y, int mouseX, int mouseY) {
        return mouseX >= x && mouseX < x + this.getWidth() && mouseY >= y && mouseY < y + this
            .getHeight();
    }

    public int getCenterOffset() {
        return (this.getWidth() / 2) - (Minecraft.getMinecraft().fontRenderer.getStringWidth(label)
            / 2);
    }
}
