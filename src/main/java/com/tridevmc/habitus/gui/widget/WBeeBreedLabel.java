/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.gui.widget;

import com.elytradev.concrete.inventory.gui.client.GuiDrawing;
import com.elytradev.concrete.inventory.gui.widget.WFieldedLabel;
import com.tridevmc.habitus.entity.EnumBeeBreed;
import com.tridevmc.habitus.tile.TileEntityApiary;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.I18n;
import net.minecraft.inventory.IInventory;

public class WBeeBreedLabel extends WFieldedLabel {

    public WBeeBreedLabel(IInventory inventory, int field, String format, int color) {
        super(inventory, field, format, color);
    }

    @Override
    public void paintBackground(int x, int y) {
        EnumBeeBreed breed = EnumBeeBreed.fromInt(this.inventory.getField(this.field));
        GuiDrawing.drawString(this.text, x, y, 0x404040);
        if (this.inventory.getField(TileEntityApiary.FIELD_BEE_COUNT) == 0) {
            GuiDrawing.drawString("N/A",
                x + Minecraft.getMinecraft().getRenderManager().getFontRenderer()
                    .getStringWidth(this.text), y, 0x6b6b6b);
        } else {
            GuiDrawing.drawString(I18n.format(breed.getName()),
                x + Minecraft.getMinecraft().getRenderManager().getFontRenderer()
                    .getStringWidth(this.text), y, breed.getColor());
        }
    }
}
