/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.gui.widget;

import com.elytradev.concrete.inventory.gui.ConcreteContainer;
import com.elytradev.concrete.inventory.gui.client.GuiDrawing;
import com.elytradev.concrete.inventory.gui.widget.WWidget;
import com.google.common.collect.Lists;
import java.util.List;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;


public class WSingleItemSlot extends WWidget {

    protected final List<Slot> peers = Lists.newArrayList();
    protected IInventory inventory;
    protected ConcreteContainer container;
    protected int startIndex = 0;
    protected int slotsWide = 1;
    protected int slotsHigh = 1;
    private boolean big = false;

    public WSingleItemSlot(IInventory inventory, int startIndex, int slotsWide, int slotsHigh,
        boolean big, boolean ltr) {
        this.inventory = inventory;
        this.startIndex = startIndex;
        this.slotsWide = slotsWide;
        this.slotsHigh = slotsHigh;
        this.big = big;
    }

    protected WSingleItemSlot() {
    }

    public static WSingleItemSlot of(IInventory inventory, int index) {
        WSingleItemSlot w = new WSingleItemSlot();
        w.inventory = inventory;
        w.startIndex = index;
        return w;
    }

    @Override
    public int getWidth() {
        return this.slotsWide * 18;
    }

    @Override
    public int getHeight() {
        return this.slotsHigh * 18;
    }

    @Override
    public void createPeers(ConcreteContainer c) {
        this.container = c;
        this.peers.clear();
        int index = this.startIndex;

        for (int y = 0; y < this.slotsHigh; ++y) {
            for (int x = 0; x < this.slotsWide; ++x) {
                ValidatedSingleSlot slot = new ValidatedSingleSlot(this.inventory, index,
                    this.getAbsoluteX() + x * 18, this.getAbsoluteY() + y * 18);
                this.peers.add(slot);
                c.addSlotPeer(slot);
                ++index;
            }
        }

    }

    @Override
    @SideOnly(Side.CLIENT)
    public void paintBackground(int x, int y) {
        for (int xi = 0; xi < this.slotsWide; ++xi) {
            for (int yi = 0; yi < this.slotsHigh; ++yi) {
                int lo = GuiDrawing.colorAtOpacity(0, 0.72F);
                int bg = GuiDrawing.colorAtOpacity(0, 0.29F);
                int hi = GuiDrawing.colorAtOpacity(16777215, 1.0F);
                if (this.container != null) {
                    lo = GuiDrawing.colorAtOpacity(0, this.container.getBevelStrength());
                    bg = GuiDrawing.colorAtOpacity(0, this.container.getBevelStrength() / 2.4F);
                    hi = GuiDrawing.colorAtOpacity(16777215, this.container.getBevelStrength());
                }

                if (this.big) {
                    GuiDrawing
                        .drawBeveledPanel(xi * 18 + x - 4, yi * 18 + y - 4, 24, 24, lo, bg, hi);
                } else {
                    GuiDrawing
                        .drawBeveledPanel(xi * 18 + x - 1, yi * 18 + y - 1, 18, 18, lo, bg, hi);
                }
            }
        }

    }
}