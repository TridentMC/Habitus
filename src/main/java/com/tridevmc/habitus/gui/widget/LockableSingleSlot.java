package com.tridevmc.habitus.gui.widget;

import com.tridevmc.habitus.gui.ILockableInventory;
import net.minecraft.entity.player.EntityPlayer;

import javax.annotation.Nullable;

public class LockableSingleSlot extends ValidatedSingleSlot {
    LockableSingleSlot(ILockableInventory inventoryIn, int index, int xPosition, int yPosition) {
        super(inventoryIn, index, xPosition, yPosition);
    }

    @Nullable
    @Override
    public String getSlotTexture() {
        return "habitus:items/empty_frame_slot";
    }

    @Override
    public boolean canTakeStack(EntityPlayer playerIn) {
        return !((ILockableInventory)inventory).isLocked();
    }
}
