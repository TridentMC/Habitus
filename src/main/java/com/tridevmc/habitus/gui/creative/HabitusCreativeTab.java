/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.gui.creative;

import com.tridevmc.habitus.init.HSBlocks;
import java.time.LocalDateTime;
import java.time.Month;
import javax.annotation.Nonnull;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

public class HabitusCreativeTab extends CreativeTabs {

    public HabitusCreativeTab(String name) {
        super(name + ((LocalDateTime.now().getMonth() == Month.APRIL
            && LocalDateTime.now().getDayOfMonth() == 1) ? "_af" : ""));
    }

    @Override
    @Nonnull
    public ItemStack getTabIconItem() {
        return new ItemStack(HSBlocks.cherrySapling);
    }

}
