package com.tridevmc.habitus.gui;

import net.minecraft.inventory.IInventory;

public interface ILockableInventory extends IInventory {
    boolean isLocked();
}
