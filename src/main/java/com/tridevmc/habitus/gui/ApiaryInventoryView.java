package com.tridevmc.habitus.gui;

import com.elytradev.concrete.inventory.ValidatedInventoryView;
import com.tridevmc.habitus.tile.ApiaryItemStorage;

public class ApiaryInventoryView extends ValidatedInventoryView implements ILockableInventory {
    private ApiaryItemStorage trueDelegate;

    public ApiaryInventoryView(ApiaryItemStorage delegate) {
        super(delegate);
        this.trueDelegate = delegate;
    }

    @Override
    public boolean isLocked() {
        return trueDelegate.framesLocked();
    }
}
