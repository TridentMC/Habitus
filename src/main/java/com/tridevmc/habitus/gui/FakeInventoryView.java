/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.gui;

import com.google.common.collect.Maps;
import java.util.Map;
import java.util.function.Supplier;
import javax.annotation.Nonnull;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentTranslation;

public class FakeInventoryView implements IInventory {

    private int[] fields = new int[0];
    private final Map<Integer, Supplier<Integer>> fieldDelegates = Maps.newHashMap();
    private final String name;

    public FakeInventoryView(String name) {
        super();
        this.name = name;
    }

    @Override
    public int getSizeInventory() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    @Nonnull
    public ItemStack getStackInSlot(int index) {
        return ItemStack.EMPTY;
    }

    @Override
    @Nonnull
    public ItemStack decrStackSize(int index, int count) {
        return ItemStack.EMPTY;
    }

    @Override
    @Nonnull
    public ItemStack removeStackFromSlot(int index) {
        return ItemStack.EMPTY;
    }

    @Override
    public void setInventorySlotContents(int index, @Nonnull ItemStack stack) {

    }

    @Override
    public int getInventoryStackLimit() {
        return 0;
    }

    @Override
    public void markDirty() {

    }

    @Override
    public boolean isUsableByPlayer(@Nonnull EntityPlayer player) {
        return true;
    }

    @Override
    public void openInventory(@Nonnull EntityPlayer player) {

    }

    @Override
    public void closeInventory(@Nonnull EntityPlayer player) {

    }

    @Override
    public boolean isItemValidForSlot(int index, @Nonnull ItemStack stack) {
        return false;
    }

    /* All borrowed from Concrete, specifically ValidatedInventoryView */

    @Override
    public int getField(int id) {
        Supplier<Integer> delegate = fieldDelegates.get(id);

        if (delegate != null) {
            return delegate.get();
        } else if (fields.length > id) {
            return fields[id];
        }

        return 0;
    }

    @Override
    public void setField(int id, int value) {
        if (fields.length <= id) {
            int[] newFields = new int[id + 1];
            if (fields.length > 0) {
                System.arraycopy(fields, 0, newFields, 0, fields.length);
            }
            fields = newFields;
        }
        fields[id] = value;
    }

    @Override
    public int getFieldCount() {
        //TODO: This is prone to problems; assumes that fieldDelegates are contiguous
        return Math.max(fields.length, fieldDelegates.size());
    }

    /* End borrowing */

    @Override
    public void clear() {

    }

    @Override
    @Nonnull
    public String getName() {
        return name;
    }

    @Override
    public boolean hasCustomName() {
        return false;
    }

    @Override
    @Nonnull
    public ITextComponent getDisplayName() {
        return new TextComponentTranslation(this.getName());
    }

    public FakeInventoryView withField(int index, Supplier<Integer> delegate) {
        fieldDelegates.put(index, delegate);
        return this;
    }


}
