/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.particle;

import com.tridevmc.habitus.entity.EnumBeeBreed;
import javax.annotation.Nullable;
import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ParticleBee extends Particle {

    public static final ResourceLocation beeTexture = new ResourceLocation(
        "habitus:entity/bee_particle");
    public static final ResourceLocation frigidBeeTexture = new ResourceLocation(
        "habitus:entity/frigid_bee_particle");
    public static final ResourceLocation aridBeeTexture = new ResourceLocation(
        "habitus:entity/arid_bee_particle");
    public static final ResourceLocation swampyBeeTexture = new ResourceLocation(
        "habitus:entity/swampy_bee_particle");
    public static final ResourceLocation hellishBeeTexture = new ResourceLocation(
        "habitus:entity/hellish_bee_particle");
    public static final ResourceLocation fungalBeeTexture = new ResourceLocation(
        "habitus:entity/fungal_bee_particle");
    public static final ResourceLocation ceramicBeeTexture = new ResourceLocation(
        "habitus:entity/ceramic_bee_particle");
    public static final ResourceLocation transcendentBeeTexture = new ResourceLocation(
        "habitus:entity/transcendent_bee_particle");
    public static final ResourceLocation humidBeeTexture = new ResourceLocation(
        "habitus:entity/humid_bee_particle");

    private final boolean directionX;
    private boolean directionZ;
    private final double speedX;
    private double speedZ;
    private BlockPos trackingPos;
    private boolean tracking = false;
    private boolean dancing = false;

    public ParticleBee(World worldIn, double xCoordIn, double yCoordIn, double zCoordIn,
        double xSpeedIn, double ySpeedIn, double zSpeedIn, EnumBeeBreed breed) {
        super(worldIn, xCoordIn, yCoordIn, zCoordIn, xSpeedIn, ySpeedIn, zSpeedIn);
        this.particleAlpha = 0.99F;
        particleRed = (float) xSpeedIn;
        particleGreen = (float) ySpeedIn;
        particleBlue = (float) zSpeedIn;
        motionX = (rand.nextDouble() - 0.5);
        motionY = (rand.nextDouble() - 0.5);
        motionZ = (rand.nextDouble() - 0.5);
        speedX = (rand.nextDouble() / 2.0);
        speedZ = 0.25;
        speedZ = (rand.nextDouble() / 2.0);
        directionX = rand.nextBoolean();
        directionZ = false;
        directionZ = rand.nextBoolean();

        TextureAtlasSprite sprite;
		switch(breed) {
			case TEMPERATE:
			default:
        sprite = Minecraft.getMinecraft().getTextureMapBlocks()
            .getAtlasSprite(beeTexture.toString());
			break;
			case FRIGID:
				sprite = Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite(frigidBeeTexture.toString());
				break;
			case ARID:
				sprite = Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite(aridBeeTexture.toString());
				break;
			case SWAMPY:
				sprite = Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite(swampyBeeTexture.toString());
				break;
			case HELLISH:
				sprite = Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite(hellishBeeTexture.toString());
				break;
			case FUNGAL:
				sprite = Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite(fungalBeeTexture.toString());
				break;
			case CERAMIC:
				sprite = Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite(ceramicBeeTexture.toString());
				break;
			case TRANSCENDENT:
				sprite = Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite(transcendentBeeTexture.toString());
				break;
			case HUMID:
				sprite = Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite(humidBeeTexture.toString());
				break;
		}
        setParticleTexture(sprite);

    }

    public ParticleBee setDancing(boolean dancing) {
        this.dancing = dancing;
        return this;
    }

    public void setTrackingPos(@Nullable BlockPos pos) {
        this.trackingPos = pos;
        this.tracking = true;
    }

    @Override
    public int getFXLayer() {
        return 1;
    }

    @Override
    public boolean shouldDisableDepth() {
        return false;
    }

    @Override
    public void onUpdate() {
        prevPosX = posX;
        prevPosY = posY;
        prevPosZ = posZ;

        particleAlpha -= 0.05f;

        if (!this.tracking) {
            double divisor = dancing ? 16.0 : 4.0;
            move((directionX ? 1 : -1) * Math.sin(motionX) / divisor, motionY / (dancing ? 2.0 : 4.0),
                (directionZ ? -1 : 1) * Math.cos(motionZ) / divisor);

            motionX += speedX;
            motionZ += speedZ;
        } else {
            motionX = ((this.trackingPos.getX() - this.posX) / 20.0);
            motionY = ((this.trackingPos.getY() - this.posY) / 20.0);
            motionZ = ((this.trackingPos.getZ() - this.posZ) / 20.0);

            move(motionX, motionY, motionZ);
        }

        if (this.onGround) {
            this.setExpired();
        }

		/*if(prevPosY == posY && motionY != 0) {
			this.setExpired();
		}

		if(prevPosZ == posZ && motionZ != 0) {
			this.setExpired();
		}

		if(prevPosX == posX && motionX != 0) {
			this.setExpired();
		}*/

        if (this.particleAlpha <= 0) {
            this.setExpired();
        }

        if (this.particleMaxAge-- <= 0) {
            this.setExpired();
        }
    }
}
