/*
 * Copyright 2018 Ethan Brooks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tridevmc.habitus.generic;

public class Color {

    public enum ColorType {
        RGB,
        RGBA
    }

    private byte red, green, blue, alpha;
    private float redF, greenF, blueF, alphaF;
    private final ColorType type;

    private static boolean isWithinByte(int b) {
        return (b >= 0 && b <= 255);
    }

    @SuppressWarnings("ConstantConditions")
    private static boolean isWithinRGBSpace(int rgb) {
        return (rgb >= 0 && rgb <= 0xFFFFFF);
    }

    @SuppressWarnings("ConstantConditions")
    private static boolean isWithinRGBASpace(int rgba) {
        return ((rgba >= 0) && (rgba <= 0xFFFFFFFF));
    }

    @SuppressWarnings("WeakerAccess")
    public Color(ColorType type, int red, int green, int blue, int alpha) {
        this.type = type;
        if (isWithinByte(red)) {
            this.red = (byte) red;
        } else {
            throw new RuntimeException("Red was expected to be > 0 && < 255 - was " + red);
        }

        if (isWithinByte(green)) {
            this.green = (byte) green;
        } else {
            throw new RuntimeException("Green was expected to be > 0 && < 255 - was " + green);
        }

        if (isWithinByte(blue)) {
            this.blue = (byte) blue;
        } else {
            throw new RuntimeException("Blue was expected to be > 0 && < 255 - was " + blue);
        }

        if (isWithinByte(alpha)) {
            this.alpha = (byte) alpha;
        } else {
            throw new RuntimeException("Alpha was expected to be > 0 && < 255 - was " + alpha);
        }
        this.calculateFractional();
    }

    public Color(ColorType type, int red, int green, int blue) {
        this(type, red, green, blue, 0xFF);
    }


    public Color(ColorType colorType, int color) {
        this.type = colorType;
        switch (colorType) {
            case RGB: {
                if (isWithinRGBSpace(color)) {
                    this.red = (byte) ((color & 0xFF0000) >> 16);
                    this.green = (byte) ((color & 0x00FF00) >> 8);
                    this.blue = (byte) ((color & 0x0000FF));
                    this.alpha = (byte) 0xFF;
                    this.calculateFractional();
                }
                break;
            }
            case RGBA: {
                if (isWithinRGBASpace(color)) {
                    this.red = (byte) ((color & 0xFF000000) >> 24);
                    this.green = (byte) ((color & 0x00FF0000) >> 16);
                    this.blue = (byte) ((color & 0x0000FF00) >> 8);
                    this.alpha = (byte) ((color & 0x000000FF));
                    this.calculateFractional();
                }
                break;
            }
        }
    }

    public Color(ColorType colorType, float red, float green, float blue) {
        this(colorType, (int) red * 0xFF, (int) green * 0xFF, (int) blue * 0xFF, 0xFF);
    }

    public Color(ColorType colorType, float red, float green, float blue, float alpha) {
        this(colorType, (int) red * 0xFF, (int) green * 0xFF, (int) blue * 0xFF,
            (int) alpha * 0xFF);
    }

    private void calculateFractional() {
        this.redF = (float) red / (float) 0xFF;
        this.greenF = (float) green / (float) 0xFF;
        this.blueF = (float) blue / (float) 0xFF;
        this.alphaF = (float) alpha / (float) 0xFF;
    }

    public int getRed() {
        return this.red;
    }

    public int getGreen() {
        return this.green;
    }

    public int getBlue() {
        return this.blue;
    }

    public int getAlpha() {
        return this.alpha;
    }

    public float getRedFractional() {
        return redF;
    }

    public float getGreenFractional() {
        return greenF;
    }

    public float getBlueFractional() {
        return blueF;
    }

    public float getAlphaFractional() {
        return alphaF;
    }


}
